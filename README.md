# ikkyu backstage application springboot frame

## 项目介绍

为WEB应用后台提供核心功能，防止后续项目开发重复做相应功能

## 核心功能

* 基础功能

    * 自动雪花ID
    * jackson序列化
    * 日志记录
    * JSR303参数校验
    * 多环境配置

* 业务功能

    * 认证授权(可自行设置开启 / 关闭)
    * 字典管理

    * 菜单管理

    * 机构管理

    * 用户管理

    * 角色管理

## 安装说明

```cmd
# 项目克隆
git clone [repository url]

# 安装依赖
mvn install

# 启动项目
pub.ikkyu.starter.IkbootStarterApplication#main()
```

## 模块介绍

### ikboot-core

**应用核心模块**

职责:

* 缓存管理

  > 根据所引入jar文件自动注入**CacheManager**并提供简易的缓存操作组件**CacheHelper**, 支持**EHCache**、**Redis**, 根据实际引入的jar包选择自动选择缓存类型, 优先级: Redis > EHCache

* Fastjson序列化管理

  > 基于**ValueFilter**实现数据脱敏处理，避免将敏感信息写入日志文件

* Mybatis插件管理

  > 基于**Interceptor**实现雪花ID的自动注入, 注意: 只有在PK字段无值时写入自动生成的雪花id

* Security安全管理

  > 提供全局对安全模块的通用操作, 包括密码加密、安全模块工具组件

* 全局便捷工具类

  > 日志、异常、Http、文件上传等

* Web应用管理

    * 文件日志切面
    * 操作人设置切面
    * fastjson入出参序列化配置
    * Web自定义异常及全局异常处理
    * 全局通用入出参实体
    * JSR303校验扩展（包括校验注解扩展、校验规则定义）

### ikboot-platform

**平台管理模块**

指责：

* 提供字典、菜单、机构、用户、角色管理功能

### ikboot-security

**应用安全模块**

> 基于Spring Security

职责:

* 用户身份认证
* 基于请求URI的授权管理

### ikboot-starter

说明:

应用启动模块

职责:

* SpringBoot应用启动类配置, 全局System Properties参数配置

* SpringBoot应用配置文件管理, 公用配置文件放在 **resources/config**目录下, 并在**application.yml**文件中通过**spring.profiles.include**属性引入
* SpringBoot应用环境配置文件管理, 环境相关配置文件存放在**resources**目录下, 参考**application-env.yml.template**文件编写
* 其他配置文件均存放到**resources**目录下，例如**ValidationMessages.properties**配置JSR303校验异常信息
