package pub.ikkyu.core.cache.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import pub.ikkyu.core.cache.CacheAttribute;
import pub.ikkyu.core.cache.CacheHelper;

import java.time.Duration;

/**
 * 自定义redis缓存配置类, 根据所引入jar包自动配置
 */
@Configuration
@ConditionalOnClass({RedisCache.class, RedisCacheManager.class})
public class CustomizerRedisCacheConfiguration {

    @Bean
    @ConditionalOnMissingBean(CacheManager.class)
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();

        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration) {
            @Override
            protected RedisCache createRedisCache(String name, RedisCacheConfiguration cacheConfig) {
                CacheAttribute cacheAttribute = CacheAttribute.parse(name);

                // 设置缓存超时时间
                RedisCacheConfiguration config = cacheConfig.entryTtl(Duration.ofSeconds(cacheAttribute.getExpire()));

                return super.createRedisCache(cacheAttribute.getName(), config);
            }
        };
    }

    /**
     * 缓存工具组件bean配置
     *
     * @param cacheManager 缓存管理器
     * @return 缓存工具组件
     */
    @Bean
    @ConditionalOnBean(CacheManager.class)
    public CacheHelper cacheHelper(CacheManager cacheManager) {
        return new CacheHelper(cacheManager);
    }

}
