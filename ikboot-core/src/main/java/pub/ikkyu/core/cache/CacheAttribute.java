package pub.ikkyu.core.cache;

import org.springframework.util.StringUtils;
import pub.ikkyu.core.web.exception.ServiceException;

import java.util.StringJoiner;

/**
 * 缓存属性实体
 *
 * @author ikkyu
 */
public class CacheAttribute {

    /**
     * 过期时间分隔符
     */
    private static final String EXPIRE_SEPARATOR = "#";

    /**
     * 缓存名称
     */
    private final String name;

    /**
     * 超时时间
     */
    private final Integer expire;

    private CacheAttribute(String name) {
        this.name = name;
        this.expire = 0;
    }

    private CacheAttribute(String name, Integer expire) {
        this.name = name;
        this.expire = expire;
    }

    public static CacheAttribute parse(String cacheAttribute) {
        if (!StringUtils.hasText(cacheAttribute)) {
            throw new RuntimeException("cache attribute can't be empty!");
        }

        int expireSeparatorIndex = -1;
        if ((expireSeparatorIndex = cacheAttribute.indexOf(EXPIRE_SEPARATOR)) != -1) {
            if (cacheAttribute.endsWith(EXPIRE_SEPARATOR)) {
                return new CacheAttribute(cacheAttribute.substring(0, expireSeparatorIndex));
            } else {
                String cacheName = cacheAttribute.substring(0, expireSeparatorIndex);
                String expireStr = cacheAttribute.substring(expireSeparatorIndex + 1);
                try {
                    Integer expire = Integer.parseInt(expireStr);
                    return new CacheAttribute(cacheName, expire);
                } catch (NumberFormatException ex) {
                    throw new ServiceException(String.format("expire must be int value, current value: %s", expireStr));
                }
            }
        } else {
            return new CacheAttribute(cacheAttribute);
        }

    }

    public String getName() {
        return name;
    }

    public Integer getExpire() {
        return expire;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CacheAttribute.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("expire=" + expire)
                .toString();
    }

}
