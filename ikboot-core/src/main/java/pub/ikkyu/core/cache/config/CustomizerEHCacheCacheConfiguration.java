package pub.ikkyu.core.cache.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pub.ikkyu.core.cache.CacheAttribute;
import pub.ikkyu.core.cache.CacheHelper;

/**
 * 自定义ehcache缓存配置类, 优先级低于redis缓存, 根据所引入jar包自动配置
 */
@Configuration
@ConditionalOnMissingClass({"org.springframework.data.redis.cache.RedisCache", "org.springframework.data.redis.cache.RedisCacheManager"})
@ConditionalOnClass({net.sf.ehcache.Cache.class, EhCacheCacheManager.class})
public class CustomizerEHCacheCacheConfiguration {

    @Bean
    @ConditionalOnMissingBean(CacheManager.class)
    public CacheManager cacheManager() {
        return new EhCacheCacheManager() {
            @Override
            protected Cache getMissingCache(String name) {

                CacheAttribute cacheAttribute = CacheAttribute.parse(name);

                super.getCacheManager().addCache(new net.sf.ehcache.Cache(name, 1000, Boolean.TRUE, Boolean.FALSE, cacheAttribute.getExpire(), cacheAttribute.getExpire()));

                return new EhCacheCache(super.getCacheManager().getCache(name));
            }
        };
    }

    /**
     * 缓存工具组件bean配置
     *
     * @param cacheManager 缓存管理器
     * @return 缓存工具组件
     */
    @Bean
    @ConditionalOnBean(CacheManager.class)
    @ConditionalOnMissingBean(CacheHelper.class)
    public CacheHelper cacheHelper(CacheManager cacheManager) {
        return new CacheHelper(cacheManager);
    }

}
