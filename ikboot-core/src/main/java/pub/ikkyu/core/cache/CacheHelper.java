package pub.ikkyu.core.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.lang.NonNull;

/**
 * 缓存辅助工具
 *
 * @author ikkyu
 */
public class CacheHelper {

    private CacheManager cacheManager;

    public CacheHelper(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    /**
     * 新增缓存
     *
     * @param cacheAttributeName 缓存属性名称
     * @param key                缓存key
     * @param value              缓存value
     */
    public void put(@NonNull String cacheAttributeName, @NonNull String key, @NonNull Object value) {
        cacheManager.getCache(cacheAttributeName).put(key, value);
    }

    /**
     * 获取缓存数据
     *
     * @param cacheAttributeName 缓存属性名称
     * @param key                缓存key
     * @return 缓存value
     */
    public Object get(@NonNull String cacheAttributeName, @NonNull String key) {
        Cache.ValueWrapper valueWrapper = cacheManager.getCache(cacheAttributeName).get(key);

        if (valueWrapper != null) {
            return valueWrapper.get();
        }
        return null;
    }

    /**
     * 移除缓存数据
     *
     * @param cacheAttributeName 缓存属性名称
     * @param key                缓存key
     */
    public void remove(@NonNull String cacheAttributeName, @NonNull String key) {
        cacheManager.getCache(cacheAttributeName).evict(key);
    }

}
