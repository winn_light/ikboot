package pub.ikkyu.core.security;

import java.util.StringJoiner;

/**
 * 用户信息数据传输对象
 *
 * @author ikkyu
 */
public class UserInfoDTO {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UserInfoDTO.class.getSimpleName() + "[", "]")
                .add("username='" + username + "'")
                .toString();
    }
}
