package pub.ikkyu.core.security;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.StringJoiner;

/**
 * 安全配置属性
 *
 * @author ikkyu
 */
@Component
@ConfigurationProperties(prefix = "ik-app.security")
public class SecurityProperties implements InitializingBean {

    /**
     * 登陆URI
     */
    private String loginUri = "/login";

    /**
     * 登出URI
     */
    private String logoutUri = "/logout";

    /**
     * TOKEN请求头键
     */
    private String tokenHeaderKey = "Authorization";

    /**
     * TOKEN缓存键前缀
     */
    private String tokenCachePrefix = "SECURITY:TOKEN";

    /**
     * token有效期(单位: s)
     */
    private Integer tokenExpireSeconds = 1800;

    /**
     * TOKEN缓存键
     */
    private String tokenCacheKey;

    /**
     * 白名单API接口, 任何人都可以访问
     */
    private Set<String> whiteApiList;

    /**
     * 认证后白名单API接口, 任何认证成功的用户都可以访问
     */
    private Set<String> authenticatedApiList;

    @Override
    public void afterPropertiesSet() {
        this.tokenCacheKey = String.format("%s#%s", this.tokenCachePrefix, this.tokenExpireSeconds);
    }

    public String getLoginUri() {
        return loginUri;
    }

    public void setLoginUri(String loginUri) {
        this.loginUri = loginUri;
    }

    public String getLogoutUri() {
        return logoutUri;
    }

    public void setLogoutUri(String logoutUri) {
        this.logoutUri = logoutUri;
    }

    public String getTokenHeaderKey() {
        return tokenHeaderKey;
    }

    public void setTokenHeaderKey(String tokenHeaderKey) {
        this.tokenHeaderKey = tokenHeaderKey;
    }

    public String getTokenCachePrefix() {
        return tokenCachePrefix;
    }

    public void setTokenCachePrefix(String tokenCachePrefix) {
        this.tokenCachePrefix = tokenCachePrefix;
    }

    public String getTokenCacheKey() {
        return tokenCacheKey;
    }

    public int getTokenExpireSeconds() {
        return tokenExpireSeconds;
    }

    public void setTokenExpireSeconds(Integer tokenExpireSeconds) {
        this.tokenExpireSeconds = tokenExpireSeconds;
    }

    public Set<String> getWhiteApiList() {
        return whiteApiList;
    }

    public void setWhiteApiList(Set<String> whiteApiList) {
        this.whiteApiList = whiteApiList;
    }

    public Set<String> getAuthenticatedApiList() {
        return authenticatedApiList;
    }

    public void setAuthenticatedApiList(Set<String> authenticatedApiList) {
        this.authenticatedApiList = authenticatedApiList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SecurityProperties.class.getSimpleName() + "[", "]")
                .add("loginUri='" + loginUri + "'")
                .add("logoutUri='" + logoutUri + "'")
                .add("tokenHeaderKey='" + tokenHeaderKey + "'")
                .add("tokenCachePrefix='" + tokenCachePrefix + "'")
                .add("tokenExpireSeconds=" + tokenExpireSeconds)
                .add("tokenCacheKey='" + tokenCacheKey + "'")
                .add("whiteApiList=" + whiteApiList)
                .add("authenticatedApiList=" + authenticatedApiList)
                .toString();
    }
}
