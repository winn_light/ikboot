package pub.ikkyu.core.security;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pub.ikkyu.core.cache.CacheHelper;

import javax.servlet.http.HttpServletRequest;

/**
 * 安全模块工具组件
 *
 * @author ikkyu
 */
@Component
public class SecurityHelper {

    @Autowired
    private SecurityProperties securityProperties;

    @Value("${ik-app.security.enable:true}")
    private Boolean securityEnabled;

    @Autowired
    private CacheHelper cacheHelper;

    /**
     * 获取当前登陆用户用户名
     */
    public String getCurrentUsername() {
        return getCurrentUser().getUsername();
    }

    /**
     * 获取当前登陆用户
     */
    public UserInfoDTO getCurrentUser() {
        if (securityEnabled) {
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = requestAttributes.getRequest();

            Object userDetails = cacheHelper.get(securityProperties.getTokenCacheKey(), request.getHeader(securityProperties.getTokenHeaderKey()));

            UserInfoDTO userInfo = new UserInfoDTO();
            BeanUtils.copyProperties(userDetails, userInfo);
            return userInfo;
            // 未启用安全认证模块时, 模拟返回当前用户
        } else {
            UserInfoDTO mockUser = new UserInfoDTO();
            mockUser.setUsername("user-mock");

            return mockUser;
        }
    }

}
