package pub.ikkyu.core.json.filter;

import com.alibaba.fastjson.serializer.ValueFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import pub.ikkyu.core.json.annotation.Desensitization;
import pub.ikkyu.core.json.constants.DesensitizationType;
import pub.ikkyu.core.utils.LogUtil;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 自定义值脱敏过滤器
 *
 * @author ikkyu
 */
@Component
public class ValueDesensitizationFilter implements ValueFilter {

    @Override
    public Object process(Object obj, String propName, Object value) {
        if (!(value instanceof String && StringUtils.hasText((String) value))) {
            return value;
        }

        // 不对单个参数做脱敏处理
        if (obj instanceof Map) {
            return value;
        }

        try {
            Field field = obj.getClass().getDeclaredField(propName);
            Desensitization desensitization;
            if (!(String.class.equals(field.getType()) && (desensitization = field.getAnnotation(Desensitization.class)) != null)) {
                return value;
            }

            DesensitizationType type = desensitization.value();
            String regexp = type.getRegexp();
            String replacement = type.getReplacement();

            return ((String) value).replaceAll(regexp, replacement);
        } catch (NoSuchFieldException e) {
            LogUtil.warn("Desensitize the class {} has no field {}", obj.getClass().getSimpleName(), propName);
        }

        return value;
    }

}
