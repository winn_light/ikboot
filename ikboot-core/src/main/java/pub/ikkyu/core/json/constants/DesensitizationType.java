package pub.ikkyu.core.json.constants;

/**
 * 数据脱敏类型
 *
 * @author ikkyu
 */
public enum DesensitizationType {
    /**
     * 密码脱敏
     */
    PASSWORD(".+", "********");

    /**
     * 正则表达式
     */
    private String regexp;

    /**
     * 替换值
     */
    private String replacement;

    DesensitizationType(String regexp, String replacement) {
        this.regexp = regexp;
        this.replacement = replacement;
    }

    public String getRegexp() {
        return regexp;
    }

    public String getReplacement() {
        return replacement;
    }
}
