package pub.ikkyu.core.json.annotation;

import pub.ikkyu.core.json.constants.DesensitizationType;

import java.lang.annotation.*;

/**
 * 脱敏标记注解
 *
 * @author ikkyu
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Desensitization {

    /**
     * 脱敏类型
     */
    DesensitizationType value();

}
