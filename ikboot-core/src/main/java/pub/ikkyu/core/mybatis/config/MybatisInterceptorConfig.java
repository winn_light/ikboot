package pub.ikkyu.core.mybatis.config;

import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pub.ikkyu.core.mybatis.interceptor.MybatisAutoSnowIdInterceptor;

/**
 * Mybatis 拦截器配置
 * 目前主要用于配置雪花id自动设置插件
 *
 * @author ikkyu
 */
@Configuration
public class MybatisInterceptorConfig {

    @Bean
    ConfigurationCustomizer mybatisConfigurationCustomizer() {
        return configuration -> configuration.addInterceptor(new MybatisAutoSnowIdInterceptor());
    }

}
