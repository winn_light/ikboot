package pub.ikkyu.core.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 雪花ID主键注解
 *
 * @author ikkyu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AutoSnowId {
}