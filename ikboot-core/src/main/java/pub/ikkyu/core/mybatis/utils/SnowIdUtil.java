package pub.ikkyu.core.mybatis.utils;

import pub.ikkyu.core.utils.LogUtil;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * 雪花算法工具类
 *
 * @author ikkyu
 */
public class SnowIdUtil {

    /**
     * 获取String类型雪花ID
     */
    public static String uniqueLongHex() {
        return String.format("%016x", SnowFlake.SNOW_FLAKE.nextId());
    }

    /**
     * 私有的 内部静态类
     */
    private static class SnowFlake {

        /**
         * 内部类对象
         */
        private static final SnowFlake SNOW_FLAKE = new SnowFlake();

        /**
         * 起始的时间戳
         */
        private final long START_TIMESTAMP = 1557489395327L;

        /**
         * 序列号占用位数
         */
        private final long SEQUENCE_BIT = 12;

        /**
         * 机器标识占用位数
         */
        private final long MACHINE_BIT = 10;

        /**
         * 时间戳位移位数
         */
        private final long TIMESTAMP_LEFT = SEQUENCE_BIT + MACHINE_BIT;

        /**
         * 最大序列号  （4095）
         */
        private final long MAX_SEQUENCE = ~(-1L << SEQUENCE_BIT);

        /**
         * 最大机器编号 （1023）
         */
        private final long MAX_MACHINE_ID = ~(-1L << MACHINE_BIT);

        /**
         * 生成id机器标识部分
         */
        private long machineIdPart;

        /**
         * 序列号
         */
        private long sequence = 0L;

        /**
         * 上一次时间戳
         */
        private long lastStamp = -1L;

        /**
         * 构造函数初始化机器编码
         */
        private SnowFlake() {
            long localIp = ipToLong(getIpAddress());
            // 本机ip地址 & MAX_MACHINE_ID最大不会超过1023,在左位移12位
            machineIdPart = (localIp & MAX_MACHINE_ID) << SEQUENCE_BIT;
        }

        /**
         * ip string 转换为 long
         *
         * @param ip 待转换ip
         */
        private static long ipToLong(String ip) {
            String[] ipArray = ip.split("\\.");
            List<Long> ipNums = new ArrayList<>();
            for (int i = 0; i < 4; ++i) {
                ipNums.add(Long.parseLong(ipArray[i].trim()));
            }

            return ipNums.get(0) * 256L * 256L * 256L + ipNums.get(1).longValue() * 256L * 256L + ipNums.get(2).longValue() * 256L + ipNums.get(3).longValue();
        }

        /**
         * 获取本机ip地址
         *
         * @return 本机ip
         */
        private static String getIpAddress() {
            try {
                Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
                InetAddress ip;
                while (allNetInterfaces.hasMoreElements()) {
                    NetworkInterface netInterface = allNetInterfaces.nextElement();
                    if (!(netInterface.isLoopback() || netInterface.isVirtual() || !netInterface.isUp())) {
                        Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                        while (addresses.hasMoreElements()) {
                            ip = addresses.nextElement();
                            if (ip instanceof Inet4Address) {
                                return ip.getHostAddress();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                LogUtil.error("IP地址获取失败: ", e);
            }
            return "";
        }

        /**
         * 获取雪花ID
         */
        public synchronized long nextId() {
            long currentStamp = timeGen();
            // 避免机器时钟回拨
            while (currentStamp < lastStamp) {
                // 服务器时钟被调整了,ID生成器停止服务.
                throw new RuntimeException(String.format("时钟已经回拨.  Refusing to generate id for %d milliseconds", lastStamp - currentStamp));
            }
            if (currentStamp == lastStamp) {
                // 每次+1
                sequence = (sequence + 1) & MAX_SEQUENCE;
                // 毫秒内序列溢出
                if (sequence == 0) {
                    // 阻塞到下一个毫秒,获得新的时间戳
                    currentStamp = getNextMill();
                }
            } else {
                // 不同毫秒内，序列号置0
                sequence = 0L;
            }
            lastStamp = currentStamp;
            // 时间戳部分+机器标识部分+序列号部分
            return (currentStamp - START_TIMESTAMP) << TIMESTAMP_LEFT | machineIdPart | sequence;
        }

        /**
         * 阻塞到下一个毫秒，直到获得新的时间戳
         */
        private long getNextMill() {
            long mill = timeGen();
            //
            while (mill <= lastStamp) {
                mill = timeGen();
            }
            return mill;
        }

        /**
         * 返回以毫秒为单位的当前时间
         */
        protected long timeGen() {
            return System.currentTimeMillis();
        }
    }

}
