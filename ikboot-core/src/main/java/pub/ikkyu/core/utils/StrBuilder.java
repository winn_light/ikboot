package pub.ikkyu.core.utils;

import java.util.Arrays;

public class StrBuilder implements CharSequence {

    /**
     * 默认容量
     */
    public static final int DEFAULT_CAPACITY = 16;

    /**
     * 存放的字符数组
     */
    private char[] value;

    /**
     * 当前指针位置，或者叫做已经加入的字符数，此位置总在最后一个字符之后
     */
    private int position;

    /**
     * 构造
     */
    public StrBuilder() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * 构造
     *
     * @param initialCapacity 初始容量
     */
    public StrBuilder(int initialCapacity) {
        value = new char[initialCapacity];
    }

    /**
     * 追加一个字符
     *
     * @param c 字符
     * @return this
     */
    public StrBuilder append(char c) {
        return insert(this.position, c);
    }

    public StrBuilder append(CharSequence csq) {
        return insert(this.position, csq);
    }

    /**
     * 插入指定字符
     *
     * @param index 位置
     * @param c     字符
     * @return this
     */
    public StrBuilder insert(int index, char c) {
        moveDataAfterIndex(index, 1);
        value[index] = c;
        this.position = Math.max(this.position, index) + 1;
        return this;
    }

    /**
     * 指定位置插入字符串的某个部分<br>
     * 如果插入位置为当前位置，则定义为追加<br>
     * 如果插入位置大于当前位置，则中间部分补充空格
     *
     * @param index 位置
     * @param csq   字符串
     * @return this
     */
    public StrBuilder insert(int index, CharSequence csq) {
        if (null == csq) {
            csq = StrUtil.EMPTY;
        }
        int len = csq.length();
        moveDataAfterIndex(index, csq.length());
        if (csq instanceof String) {
            ((String) csq).getChars(0, len, this.value, index);
        } else if (csq instanceof StringBuilder) {
            ((StringBuilder) csq).getChars(0, len, this.value, index);
        } else if (csq instanceof StringBuffer) {
            ((StringBuffer) csq).getChars(0, len, this.value, index);
        } else if (csq instanceof StrBuilder) {
            ((StrBuilder) csq).getChars(0, len, this.value, index);
        } else {
            for (int i = 0, j = this.position; i < len; i++, j++) {
                this.value[j] = csq.charAt(i);
            }
        }
        this.position = Math.max(this.position, index) + len;
        return this;
    }

    /**
     * 指定位置之后的数据后移指定长度
     *
     * @param index  位置
     * @param length 位移长度
     */
    private void moveDataAfterIndex(int index, int length) {
        ensureCapacity(Math.max(this.position, index) + length);
        if (index < this.position) {
            // 插入位置在已有数据范围内，后移插入位置之后的数据
            System.arraycopy(this.value, index, this.value, index + length, this.position - index);
        } else if (index > this.position) {
            // 插入位置超出范围，则当前位置到index清除为空格
            Arrays.fill(this.value, this.position, index, StrUtil.C_SPACE);
        }
        // 不位移
    }

    /**
     * 确认容量是否够用，不够用则扩展容量
     *
     * @param minimumCapacity 最小容量
     */
    private void ensureCapacity(int minimumCapacity) {
        // overflow-conscious code
        if (minimumCapacity - value.length > 0) {
            expandCapacity(minimumCapacity);
        }
    }

    /**
     * 扩展容量<br>
     * 首先对容量进行二倍扩展，如果小于最小容量，则扩展为最小容量
     *
     * @param minimumCapacity 需要扩展的最小容量
     */
    private void expandCapacity(int minimumCapacity) {
        int newCapacity = (value.length << 1) + 2;
        // overflow-conscious code
        if (newCapacity - minimumCapacity < 0) {
            newCapacity = minimumCapacity;
        }
        if (newCapacity < 0) {
            throw new OutOfMemoryError("Capacity is too long and max than Integer.MAX");
        }
        value = Arrays.copyOf(value, newCapacity);
    }

    /**
     * 将指定段的字符列表写出到目标字符数组中
     *
     * @param srcBegin 起始位置（包括）
     * @param srcEnd   结束位置（不包括）
     * @param dst      目标数组
     * @param dstBegin 目标起始位置（包括）
     * @return this
     */
    public StrBuilder getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin) {
        if (srcBegin < 0) {
            srcBegin = 0;
        }
        if (srcEnd < 0) {
            srcEnd = 0;
        } else if (srcEnd > this.position) {
            srcEnd = this.position;
        }
        if (srcBegin > srcEnd) {
            throw new StringIndexOutOfBoundsException("srcBegin > srcEnd");
        }
        System.arraycopy(value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
        return this;
    }

    @Override
    public int length() {
        return this.position;
    }

    @Override
    public char charAt(int index) {
        if (index < 0) {
            index = this.position + index;
        }
        if ((index < 0) || (index > this.position)) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return this.value[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return subString(start, end);
    }

    /**
     * 返回自定段的字符串
     *
     * @param start 开始位置（包括）
     * @param end   结束位置（不包括）
     * @return this
     */
    public String subString(int start, int end) {
        return new String(this.value, start, end - start);
    }

    /**
     * 生成字符串
     */
    @Override
    public String toString() {
        return toString(false);
    }

    /**
     * 生成字符串
     *
     * @param isReset 是否重置，重置后相当于空的构建器
     * @return 生成的字符串
     */
    public String toString(boolean isReset) {
        if (position > 0) {
            final String s = new String(value, 0, position);
            if (isReset) {
                reset();
            }
            return s;
        }
        return StrUtil.EMPTY;
    }

    /**
     * 删除全部字符，位置归零
     *
     * @return this
     */
    public StrBuilder reset() {
        this.position = 0;
        return this;
    }

}
