package pub.ikkyu.core.utils;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * HTTP工具
 *
 * @author ikkyu
 */
public class HttpUtil {

    public static void response(HttpServletResponse response, String jsonValue) {
        if (response == null || !StringUtils.hasText(jsonValue)) {
            LogUtil.error("response can't be null and json value can't be empty!");
            return;
        }

        response.setContentType("application/json;charset=UTF-8");

        try {
            response.getWriter().write(jsonValue);
        } catch (IOException ex) {
            LogUtil.error("response json data failed: ", ex);
        }
    }

}
