package pub.ikkyu.core.utils;

import org.springframework.boot.system.ApplicationHome;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import pub.ikkyu.core.web.exception.ServiceException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件处理工具类
 *
 * @author ikkyu
 */
public class FileUtil {

    /**
     * 文件上传根路径,默认上传到jar包所在的同级目录,开发环境下上传在项目下的一级目录
     * 文件路径规则: jarFilePath/../annex/[moduleCode]/yy/MM/dd/[simpleuuid].[suffix]
     * 模块编码请自行制定,不重复即可
     */
    public static final String ROOT_PATH;
    private static final String FILE_SEPARATOR = "/";

    static {
        ApplicationHome applicationHome = new ApplicationHome();
        File jarFile = applicationHome.getDir();
        // 文件统一存放到annex目录,各模块附件自行制定目录名称
        ROOT_PATH = jarFile.toString() + "/attachments/";
    }

    /**
     * 上传文件至服务器
     *
     * @param moduleCode 模块编码,作为目录名称区分各模块附件
     * @param file       待上传文件
     * @return 文件路径 - 相对于项目根目录的路径,示例: /annex/contract/2020/11/10/xxx.xlsx
     */
    public static String uploadFile(String moduleCode, MultipartFile file) {
        if (StringUtils.isEmpty(moduleCode)) {
            throw new ServiceException("文件上传失败 - 模块编号不允许为空!");
        }
        if (file == null || file.getSize() == 0) {
            throw new ServiceException("文件上传失败 - 不允许上传空文件!");
        }

        String randomFileName = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String serverPath;
        try {
            serverPath = createDir(moduleCode) + randomFileName;

            File serverFile = new File(serverPath);
            file.transferTo(serverFile);
        } catch (Exception e) {
            LogUtil.error("文件上传至服务器失败,失败原因: {}", e.getMessage(), e);
            throw new ServiceException("文件上传失败 - I/O处理异常,请稍后重试!");
        }

        return FILE_SEPARATOR + serverPath.replace(ROOT_PATH, "");
    }

    /**
     * 按模块编码创建文件夹
     *
     * @param moduleCode 模块编码
     * @return 文件夹路径, 末尾为 /
     */
    private static String createDir(String moduleCode) {
        if (StringUtils.isEmpty(moduleCode)) {
            throw new IllegalArgumentException("module code can't be null!");
        }

        SimpleDateFormat fmt = new SimpleDateFormat(String.format("yyyy%sMM%sdd%s", FILE_SEPARATOR, FILE_SEPARATOR, FILE_SEPARATOR));
        String dateDirPath = fmt.format(new Date());
        String serverPath = ROOT_PATH + moduleCode + FILE_SEPARATOR + dateDirPath;

        File folder = new File(serverPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return serverPath;
    }

}
