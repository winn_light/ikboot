package pub.ikkyu.core.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

import static pub.ikkyu.core.utils.StrUtil.EMPTY;

/**
 * 异常工具类
 *
 * @author ikkyu
 */
public class ExceptionUtil {

    /**
     * 获取异常堆栈信息
     */
    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        try {
            throwable.printStackTrace(pw);
            return sw.toString();
        } catch (Exception e) {
            // ignore
        } finally {
            pw.close();
        }

        return EMPTY;
    }

}
