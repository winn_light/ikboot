package pub.ikkyu.core.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pub.ikkyu.core.json.filter.ValueDesensitizationFilter;
import pub.ikkyu.core.security.SecurityHelper;
import pub.ikkyu.core.web.aspect.FileLoggingAspect;
import pub.ikkyu.core.web.aspect.OperatorSetAspect;
import pub.ikkyu.core.web.aspect.SortFieldSetAspect;

/**
 * 全局切面配置类
 *
 * @author ikkyu
 */
@Configuration
public class AspectConfiguration {

    @Autowired
    private SecurityHelper securityHelper;

    @Autowired
    private ValueDesensitizationFilter valueDesensitizationFilter;

    @Bean
    public FileLoggingAspect fileLoggingAspect() {
        return new FileLoggingAspect(valueDesensitizationFilter);
    }

    @Bean
    public OperatorSetAspect operatorSetAspect() {
        return new OperatorSetAspect(securityHelper);
    }

    @Bean
    public SortFieldSetAspect sortFieldSetAspect() {
        return new SortFieldSetAspect();
    }

}