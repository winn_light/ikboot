package pub.ikkyu.core.web.entity;

import java.util.List;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.ResponseCode.SUCCESS;

/**
 * label: 分页统一响应结果
 *
 * @author ikkyu
 */
public class PageResult<T> {

    /**
     * label: 响应结果编码
     * notes: 200-正常,400-服务异常,401-认证异常,405-鉴权异常,406-参数校验异常,500-预期外异常
     */
    private String code;

    /**
     * label: 总条数
     */
    private Long total;

    /**
     * label: 响应数据集
     */
    private List<T> data;

    private PageResult() {

    }

    /**
     * 正常响应 - 有数据
     *
     * @param <T>   列表项数据类型
     * @param total 数据总条数
     * @param data  数据列表
     * @return 统一分页响应结果
     */
    public static <T> PageResult<T> success(Long total, List<T> data) {
        PageResult<T> result = new PageResult<>();
        result.setCode(SUCCESS.getCode());
        result.setTotal(total);
        result.setData(data);

        return result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PageResult.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("total=" + total)
                .add("data=" + data)
                .toString();
    }

}
