package pub.ikkyu.core.web.entity;

import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.ResponseCode.SUCCESS;

/**
 * label: 统一响应结果
 *
 * @author ikkyu
 */
public class AjaxResult<T> {

    /**
     * label: 响应结果编码
     * notes: 200-正常,400-服务异常,401-认证异常,405-鉴权异常,406-参数校验异常,500-预期外异常
     *
     * @see pub.ikkyu.core.web.constants.ResponseCode
     */
    private String code;

    /**
     * label: 提示信息
     * notes: 响应状态非200时,此字段必定有值
     */
    private String message;

    /**
     * label: 响应数据
     */
    private T data;

    private AjaxResult() {
    }

    /**
     * 正常响应 - 无数据
     *
     * @return 统一响应结果
     */
    public static AjaxResult<Void> success() {
        AjaxResult<Void> result = new AjaxResult<>();
        result.setCode(SUCCESS.getCode());

        return result;
    }

    /**
     * 正常响应 - 有数据
     *
     * @return 统一响应结果
     */
    public static <T> AjaxResult<T> success(T data) {
        AjaxResult<T> result = new AjaxResult<>();
        result.setCode(SUCCESS.getCode());
        result.setData(data);

        return result;
    }

    /**
     * 异常响应
     *
     * @param code    响应结果编码
     * @param message 异常信息
     * @param data    异常响应数据
     * @param <T>     响应数据类型
     * @return 统一响应结果
     */
    public static <T> AjaxResult<T> error(String code, String message, T data) {
        AjaxResult<T> result = new AjaxResult<>();
        result.setCode(code);
        result.setMessage(message);
        result.setData(data);

        return result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AjaxResult.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("message='" + message + "'")
                .add("data=" + data)
                .toString();
    }
}
