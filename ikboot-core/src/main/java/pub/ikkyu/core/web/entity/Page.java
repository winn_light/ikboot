package pub.ikkyu.core.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.SortType;

/**
 * label: 分页参数
 *
 * @author ikkyu
 */
public class Page {

    /**
     * label: 当前页数
     */
    @NotNull(message = "{core.page.page-num.not-null.message}")
    @Min(value = 1, message = "{core.page.page-num.min.message}")
    private Integer pageNum;

    /**
     * label: 分页大小
     */
    @NotNull(message = "{core.page.page-size.not-null.message}")
    @Min(value = 1, message = "{core.page.page-size.min.message}")
    private Integer pageSize;

    /**
     * label: 排序字段名
     */
    private String sortBy;

    /**
     * label: 排序类型
     */
    @ValidDictValue(SortType.class)
    private String sortType;

    /**
     * label: 排序字符串
     */
    @JsonIgnore
    private String sortStr;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public String getSortStr() {
        return sortStr;
    }

    public void setSortStr(String sortStr) {
        this.sortStr = sortStr;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Page.class.getSimpleName() + "[", "]")
                .add("pageNum=" + pageNum)
                .add("pageSize=" + pageSize)
                .add("sortBy='" + sortBy + "'")
                .add("sortType='" + sortType + "'")
                .add("sortStr='" + sortStr + "'")
                .toString();
    }
}