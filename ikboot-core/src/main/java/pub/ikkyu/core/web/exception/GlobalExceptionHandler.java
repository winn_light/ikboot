package pub.ikkyu.core.web.exception;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import pub.ikkyu.core.json.filter.ValueDesensitizationFilter;
import pub.ikkyu.core.utils.ExceptionUtil;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.entity.AjaxResult;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;

/**
 * 全局异常处理
 *
 * @author ikkyu
 */
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @Autowired
    private ValueDesensitizationFilter valueDesensitizationFilter;

    @Value("${ik-app.debug.enable:false}")
    private Boolean debugEnabled;

    /**
     * 系统已知服务异常统一处理
     * 注意: 抛出此类异常,异常发生点应自行记录日志
     *
     * @param ex 服务异常
     * @return 统一JSON返回
     */
    @ExceptionHandler(ServiceException.class)
    public AjaxResult<Void> resolveServiceException(ServiceException ex) {
        return AjaxResult.error(ex.getCodeValue(), ex.getMessage(), null);
    }

    /**
     * 参数校验异常处理（只针对JavaBean参数异常进行处理）
     */
    @ExceptionHandler(BindException.class)
    public AjaxResult<Void> beanParamValidateExceptionHandler(HttpServletRequest request, BindException e) {
        LogUtil.info("================= request divider =================");
        LogUtil.info("请求信息: uri--{}, method--{}, args--{}", request.getRequestURI(), request.getMethod(), JSON.toJSONString(e.getBindingResult().getTarget(), valueDesensitizationFilter, SerializerFeature.IgnoreErrorGetter, SerializerFeature.IgnoreNonFieldGetter));

        List<FieldError> errors = e.getFieldErrors();
        StringBuilder errorMessage = new StringBuilder();
        for (FieldError error : errors) {
            errorMessage.append(error.getField());
            errorMessage.append(": ");
            errorMessage.append(error.getDefaultMessage());
            errorMessage.append(", ");
        }

        AjaxResult<Void> result = AjaxResult.error(ResponseCode.PARAM_VALIDATE_ERROR.getCode(), errorMessage.substring(0, errorMessage.length() - 2), null);

        LogUtil.error("异常信息: response code--{}, message--{}, stack--{}", result.getCode(), result.getMessage(), ExceptionUtil.getStackTrace(e));
        LogUtil.error("================= request failed =================");

        return result;
    }

    /**
     * 参数校验异常处理（只针对单参数异常进行处理）
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public AjaxResult<Void> singleParamValidateFailureExceptionHandler(HttpServletRequest request, ConstraintViolationException e) {
        LogUtil.info("================= request divider =================");
        String paramStr = JSON.toJSONString(request.getParameterMap(), SerializerFeature.IgnoreErrorGetter, SerializerFeature.IgnoreNonFieldGetter);
        LogUtil.info("请求信息: uri--{}, method--{}, args--{}", request.getRequestURI(), request.getMethod(), paramStr);

        Set<ConstraintViolation<?>> errors = e.getConstraintViolations();
        StringBuilder errorMessage = new StringBuilder();
        for (ConstraintViolation<?> error : errors) {
            errorMessage.append(((PathImpl) error.getPropertyPath()).getLeafNode().asString());
            errorMessage.append(": ");
            errorMessage.append(error.getMessageTemplate());
            errorMessage.append(", ");
        }

        AjaxResult<Void> result = AjaxResult.error(ResponseCode.PARAM_VALIDATE_ERROR.getCode(), errorMessage.substring(0, errorMessage.length() - 2), null);

        LogUtil.error("response code--{}, message--{}, stack--{}", result.getCode(), result.getMessage(), ExceptionUtil.getStackTrace(e));
        LogUtil.error("================= request failed =================");

        return result;
    }

    /**
     * 未知异常处理器
     */
    @ExceptionHandler(Throwable.class)
    public AjaxResult<Void> unexpectedExceptionHandler(Throwable ex) {
        if (debugEnabled) {
            return AjaxResult.error(ResponseCode.UNEXPECTED_ERROR.getCode(), ex.getMessage(), null);
        } else {
            return AjaxResult.error(ResponseCode.UNEXPECTED_ERROR.getCode(), "未知错误, 请联系超管协助处理!", null);
        }
    }

}