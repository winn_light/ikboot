package pub.ikkyu.core.web.entity;

import java.util.List;

/**
 * 树型接口父级接口
 *
 * @author ikkyu
 */
public interface Tree {

    String getId();

    String getParentId();

    <T extends Tree> List<T> getChildren();

}
