package pub.ikkyu.core.web.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;
import pub.ikkyu.core.security.SecurityHelper;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.utils.StrUtil;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.exception.ServiceException;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;

/**
 * 操作人信息设置切面
 *
 * @author ikkyu
 */
@Aspect
public class OperatorSetAspect {

    private SecurityHelper securityHelper;

    public OperatorSetAspect(SecurityHelper securityHelper) {
        this.securityHelper = securityHelper;
    }

    @Pointcut("@annotation(pub.ikkyu.core.web.aspect.annotation.OperateInfoSet)")
    public void operateInfoSetPointcut() {
    }

    @Before(value = "operateInfoSetPointcut()&&@annotation(operateInfoSet)")
    public void doBefore(JoinPoint point, OperateInfoSet operateInfoSet) {
        String paramName = operateInfoSet.paramName();
        String operatorProp = operateInfoSet.value().getOperatorProp();
        String operatedTimeProp = operateInfoSet.value().getOperatedTimeProp();

        Object param = getParamByName(paramName, point);

        setOperatorInfo(param, operatorProp, operatedTimeProp);
    }

    /**
     * 按名称获取参数
     */
    private Object getParamByName(String paramName, JoinPoint point) {
        String[] paramNames = ((CodeSignature) point.getSignature()).getParameterNames();
        Object[] paramValues = point.getArgs();

        // 如果未配置参数名称, 则取方法的第一个参数
        if ("".equals(paramName) && paramNames.length == 1) {
            return paramValues[0];
        }

        for (int index = 0; index < paramNames.length; index++) {
            if (paramName.equals(paramNames[index])) {
                return paramValues[index];
            }
        }

        throw new ServiceException(String.format("设置操作人信息错误 - 未找到名称:%s的参数, 请检查参数名配置!", paramName));
    }

    /**
     * 设置参数操作人信息
     */
    private void setOperatorInfo(Object param, String operatorProp, String operatedTimeProp) {
        String operatorSetMethodName = StrUtil.upperFirstAndPrefix(operatorProp, "set");
        String operatedTimeSetMethodName = StrUtil.upperFirstAndPrefix(operatedTimeProp, "set");
        Method operatorSetMethod;
        Method operatedTimeSetMethod;

        // 如果当前参数是集合类型,则针对集合中的每一个项进行设置
        if (param instanceof Collection && !CollectionUtils.isEmpty((Collection<?>) param)) {
            Object paramItem = ((Collection<?>) param).iterator().next();
            operatorSetMethod = ReflectionUtils.findMethod(paramItem.getClass(), operatorSetMethodName);
            operatedTimeSetMethod = ReflectionUtils.findMethod(paramItem.getClass(), operatedTimeSetMethodName);

            ((Collection<?>) param).forEach(item -> {
                setItemOperatorInfo(item, operatorSetMethod, operatedTimeSetMethod);
            });
        } else {
            operatorSetMethod = ReflectionUtils.findMethod(param.getClass(), operatorSetMethodName, String.class);
            operatedTimeSetMethod = ReflectionUtils.findMethod(param.getClass(), operatedTimeSetMethodName, Date.class);

            setItemOperatorInfo(param, operatorSetMethod, operatedTimeSetMethod);
        }
    }

    /**
     * 设置单个参数操作信息
     */
    private void setItemOperatorInfo(Object item, Method operatorSetMethod, Method operatedTimeSetMethod) {
        if (operatorSetMethod == null || operatedTimeSetMethod == null) {
            throw new ServiceException("缺失操作人方法定义!");
        }

        try {
            ReflectionUtils.invokeMethod(operatorSetMethod, item, securityHelper.getCurrentUsername());
            ReflectionUtils.invokeMethod(operatedTimeSetMethod, item, new Date());
        } catch (Exception ex) {
            LogUtil.error("设置操作人或操作时间错误!", ex);
            throw new ServiceException(String.format("设置操作人信息错误 - 错误信息: %s", ex.getMessage()));
        }
    }

}
