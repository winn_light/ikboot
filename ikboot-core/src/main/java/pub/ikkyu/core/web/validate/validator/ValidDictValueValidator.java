package pub.ikkyu.core.web.validate.validator;

import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * 自定义有效字典项校验器
 *
 * @author ikkyu
 */
public class ValidDictValueValidator implements ConstraintValidator<ValidDictValue, Object> {

    /**
     * 是否必填项
     */
    private boolean required;

    /**
     * 字典类
     */
    private Class<?> dictClass;

    @Override
    public void initialize(ValidDictValue constraintAnnotation) {
        this.required = constraintAnnotation.required();
        this.dictClass = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (!this.required && o == null) {
            return true;
        }

        if (this.required && o == null) {
            return false;
        }

        Field[] fileds = dictClass.getDeclaredFields();

        for (Field field : fileds) {
            // 排除非常量字段校验
            if (!Modifier.isStatic(field.getModifiers()) || !Modifier.isPublic(field.getModifiers()) || !Modifier.isFinal(field.getModifiers())) {
                continue;
            }

            try {
                // 常量值不需要引入对象
                Object value = field.get(null);

                if (o.equals(value)) {
                    return true;
                }
            } catch (IllegalAccessException e) {
                LogUtil.error("是否字典项校验出错,错误原因:无法获取字段值,错误信息: {}", e.getMessage());
            }
        }

        return false;
    }

}
