package pub.ikkyu.core.web.entity;

import javax.validation.Valid;
import java.util.List;
import java.util.StringJoiner;

/**
 * label: 数组参数
 *
 * @param <T> 集合项类型
 * @author ikkyu
 */
public class ListParam<T> {

    /**
     * label: 数据列表
     */
    @Valid
    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ListParam.class.getSimpleName() + "[", "]")
                .add("list=" + list)
                .toString();
    }
}
