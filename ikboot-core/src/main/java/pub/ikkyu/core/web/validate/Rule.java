package pub.ikkyu.core.web.validate;

/**
 * 参数校验规则组
 *
 * @author ikkyu
 */
public interface Rule {

    /**
     * 新增操作校验规则组
     */
    interface Insert {
    }

    /**
     * 更新操作校验规则组
     */
    interface Update {
    }

    /**
     * 删除操作校验规则组
     */
    interface Delete {
    }

    /**
     * 数据查询操作校验规则组
     */
    interface Query {
    }

    /**
     * 状态更新校验规则组
     */
    interface UpdateStatus {
    }

    /**
     * 数据列表查询操作校验规则组
     */
    interface ListQuery {
    }

}
