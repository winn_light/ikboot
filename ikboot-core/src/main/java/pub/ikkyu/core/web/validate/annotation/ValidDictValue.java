package pub.ikkyu.core.web.validate.annotation;

import pub.ikkyu.core.web.validate.validator.ValidDictValueValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 自定义有效字典值校验注解
 *
 * @author ikkyu
 */
@Target({FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {ValidDictValueValidator.class})
public @interface ValidDictValue {

    /**
     * 是否必填
     */
    boolean required() default false;

    /**
     * 字典类 - 校验时会读取该类下的所有常量值作为有效字典项组
     */
    Class<?> value();

    /**
     * 校验失败时返回的异常信息
     */
    String message() default "{core.validator.is-dict-item.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
