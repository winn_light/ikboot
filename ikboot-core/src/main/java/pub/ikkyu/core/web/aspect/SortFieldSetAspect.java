package pub.ikkyu.core.web.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.utils.StrUtil;
import pub.ikkyu.core.web.aspect.annotation.SortField;
import pub.ikkyu.core.web.entity.Page;

import java.lang.reflect.Field;

/**
 * 排序字段设置切面
 * 用于将排序属性&排序类型替换为 order by {sort_field} {sort_type} 子句
 *
 * @author ikkyu
 */
@Aspect
public class SortFieldSetAspect {

    /**
     * 切含有 {@link Page} 参数的方法
     */
    @Pointcut("execution(* pub.ikkyu..service.impl.*.*(.., pub.ikkyu.core.web.entity.Page, ..))")
    public void sortFieldSetPointcut() {
    }

    @Before("sortFieldSetPointcut()")
    public void doBefore(JoinPoint point) {
        // 获取分页参数
        Page page = null;
        Object[] args = point.getArgs();
        for (Object arg : args) {
            if (arg instanceof Page) {
                page = (Page) arg;
                break;
            }
        }

        // 获取查询条件对象
        Object condition = getConditionParam(point);
        if (condition == null) {
            LogUtil.error("sort string dispose failed, can't found named 'condition''s params, method: {}", point.getSignature().getName());
            return;
        }

        // 如果排序字段、排序类型已传入, 则生成排序字符串
        if (page != null && !StringUtils.isEmpty(page.getSortBy()) && !StringUtils.isEmpty(page.getSortType())) {
            Field sortProperty = null;
            if ((sortProperty = ReflectionUtils.findField(condition.getClass(), page.getSortBy())) == null) {
                LogUtil.error("sort string dispose failed, named '{}' field can't be found in {}", page.getSortBy(), condition.getClass());
                return;
            }

            // 默认处理规则, 字段有@SortField注解, 则使用注解值作为排序字段名称, 否则使用原字段名称转换为下划线分割字符串作为排序字段名称
            String sortFieldName;
            SortField sortField = sortProperty.getAnnotation(SortField.class);
            if (sortField != null) {
                sortFieldName = sortField.value();
            } else {
                LogUtil.debug("sort property missing @SortField, dispose with default rule: hump to underscore");
                sortFieldName = StrUtil.toSymbolCase(sortProperty.getName(), '_');
            }

            // pagehelper sort str: {field_name} {sort_type}
            page.setSortStr(sortFieldName + " " + page.getSortType());
        }
    }

    /**
     * 获取查询条件参数
     *
     * @param point 切点
     * @return 查询条件参数对象
     */
    private Object getConditionParam(JoinPoint point) {
        // 获取参数名称
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] paramNames = methodSignature.getParameterNames();

        Object[] paramValues = point.getArgs();

        // 组装请求参数
        for (int index = 0; index < paramNames.length; index++) {
            if ("condition".equals(paramNames[index])) {
                return paramValues[index];
            }
        }

        return null;
    }

}
