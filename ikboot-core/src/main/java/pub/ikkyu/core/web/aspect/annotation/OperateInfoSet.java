package pub.ikkyu.core.web.aspect.annotation;

import pub.ikkyu.core.web.aspect.constants.OperatorType;

import java.lang.annotation.*;

/**
 * 操作信息设置注解
 * 支持参数类型: 实体参数、实体集合参数
 * 注意: 此注解只适用于服务层操作类型接口记录操作人及操作时间信息
 *
 * @author ikkyu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OperateInfoSet {

    /**
     * 如果是单个参数, 则系统自动识别
     */
    String paramName() default "";

    OperatorType value();

}
