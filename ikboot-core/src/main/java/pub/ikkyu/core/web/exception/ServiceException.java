package pub.ikkyu.core.web.exception;

import pub.ikkyu.core.web.constants.ResponseCode;

/**
 * 自定义服务处理异常
 * 注: 抛出此异常时, 需要在异常点记录详细日志信息
 *
 * @author ikkyu
 */
public class ServiceException extends RuntimeException {

    /**
     * HTTP 响应编码
     */
    private final ResponseCode code;

    /**
     * 服务处理异常抛出
     *
     * @param message 异常提示信息
     */
    public ServiceException(String message) {
        super(message);
        this.code = ResponseCode.SERVICE_ERROR;
    }

    /**
     * 其他异常抛出
     *
     * @param code    响应编码
     * @param message 异常提示信息
     */
    public ServiceException(ResponseCode code, String message) {
        super(message);
        this.code = code;
    }

    public String getCodeValue() {
        return code.getCode();
    }

}
