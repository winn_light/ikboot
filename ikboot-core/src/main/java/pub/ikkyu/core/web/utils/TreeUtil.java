package pub.ikkyu.core.web.utils;

import com.google.common.collect.Sets;
import org.springframework.util.CollectionUtils;
import pub.ikkyu.core.web.entity.Tree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 树型结构处理工具类
 *
 * @author ikkyu
 */
public class TreeUtil {

    /**
     * 数据集合转换为树集合
     *
     * @param dataList 数据集合
     * @param <T>      源集合元素类型
     * @return 树型结构集合
     */
    public static <T extends Tree> List<T> castToTreeList(List<T> dataList) {
        if (CollectionUtils.isEmpty(dataList)) {
            return new ArrayList<>(0);
        }

        // 求当前分组列表的所有根节点ID
        Set<String> idSet = new HashSet<>(dataList.size());
        Set<String> parentIdSet = new HashSet<>(dataList.size());
        for (T t : dataList) {
            idSet.add(t.getId());
            parentIdSet.add(t.getParentId());
        }
        Set<String> rootIdSet = Sets.difference(parentIdSet, idSet);

        return createTree(rootIdSet, dataList);
    }

    /**
     * 创建树形结构数据集合
     *
     * @param rootIdSet 根节点ID集合
     * @param dataList  源数据集合
     * @param <T>       源集合元素类型
     * @return 树形结构数据集合
     */
    private static <T extends Tree> List<T> createTree(Set<String> rootIdSet, List<T> dataList) {
        List<T> treeList = new ArrayList<>();
        for (T item : dataList) {
            if (rootIdSet.contains(item.getParentId())) {
                treeList.add(item);
                item.getChildren().addAll(createTree(Sets.newHashSet(item.getId()), dataList));
            }
        }
        return treeList;
    }

}
