package pub.ikkyu.core.web.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import pub.ikkyu.core.json.filter.ValueDesensitizationFilter;
import pub.ikkyu.core.utils.ExceptionUtil;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

import static pub.ikkyu.core.utils.StrUtil.EMPTY;

/**
 * 文件日志记录切面
 * 注意: 此类只能记录已进入controller层执行的日志,未进入controller层的日志无法记录(例如spring-validation抛出的400错误)
 *
 * @author ikkyu
 */
@Aspect
public class FileLoggingAspect {

    /**
     * 数据脱敏过滤器
     */
    private ValueDesensitizationFilter valueDesensitizationFilter;
    /**
     * 计时器
     */
    private ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public FileLoggingAspect(ValueDesensitizationFilter valueDesensitizationFilter) {
        this.valueDesensitizationFilter = valueDesensitizationFilter;
    }

    @Pointcut("execution(* pub.ikkyu..controller.*.*(..))")
    public void fileLoggingPointcut() {
    }

    @Before("fileLoggingPointcut()")
    public void doBefore(JoinPoint point) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        LogUtil.info("================= request divider =================");

        String paramStr = EMPTY;
        try {
            paramStr = JSON.toJSONString(
                    // 获取包含参数名称的请求参数集合
                    getRequestParams(point),
                    // 数据脱敏
                    valueDesensitizationFilter,
                    // 输出null值key
                    SerializerFeature.WriteMapNullValue,
                    SerializerFeature.IgnoreErrorGetter,
                    SerializerFeature.IgnoreNonFieldGetter);
        } catch (Exception ex) {
            LogUtil.warn("日志切面记录请求参数失败: ", ex);
        }

        LogUtil.info("请求信息: uri--{}, method--{}, args--{}", request.getRequestURI(), request.getMethod(), paramStr);

        // 记录请求接口开始时间
        threadLocal.set(System.currentTimeMillis());
    }

    @AfterReturning(value = "fileLoggingPointcut()", returning = "result")
    public void doAfterReturning(JoinPoint point, Object result) {
        String resultStr = result == null ? "null" : result.toString();
        Long responseDuration = System.currentTimeMillis() - threadLocal.get();

        LogUtil.info("响应信息: result--{}, duration--{}ms", resultStr, responseDuration);
        LogUtil.info("================= request completed =================");

        // 删除threadLocal变量副本
        threadLocal.remove();
    }

    @AfterThrowing(value = "fileLoggingPointcut()", throwing = "exception")
    public void doAfterThrowing(Throwable exception) {
        String exCode;
        String stackTrace = EMPTY;

        if (exception instanceof ServiceException) {
            exCode = ((ServiceException) exception).getCodeValue();
        } else {
            exCode = ResponseCode.UNEXPECTED_ERROR.getCode();
            stackTrace = ExceptionUtil.getStackTrace(exception);
        }
        String exMessage = exception.getMessage();

        LogUtil.error("异常信息: response code--{}, message--{}, stack--{}", exCode, exMessage, stackTrace);
        LogUtil.error("================= request failed =================");
    }

    /**
     * 获取请求参数集合
     *
     * @param point 切点
     * @return 请求参数集合
     */
    private Map<String, Object> getRequestParams(JoinPoint point) {
        // 获取参数名称
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] paramNames = methodSignature.getParameterNames();

        Object[] paramValues = point.getArgs();

        // 组装请求参数
        Map<String, Object> params = new LinkedHashMap<>(paramNames.length);
        for (int index = 0; index < paramNames.length; index++) {
            // 不记录文件参数信息
            if (paramValues[index] instanceof MultipartFile) {
                continue;
            }
            params.put(paramNames[index], paramValues[index]);
        }

        return params;
    }

}
