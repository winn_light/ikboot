package pub.ikkyu.core.web.constants;

/**
 * 业务常量
 *
 * @author ikkyu
 */
public interface BusinessConstant {

    /**
     * 树节点根ID
     */
    String TREE_ROOT_ID = "0";

}
