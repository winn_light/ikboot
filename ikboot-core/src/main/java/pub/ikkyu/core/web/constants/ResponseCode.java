package pub.ikkyu.core.web.constants;

/**
 * HTTP 响应编码
 *
 * @author ikkyu
 */
public enum ResponseCode {

    /**
     * 处理成功
     */
    SUCCESS("200"),
    /**
     * 服务处理异常 - 已知异常
     */
    SERVICE_ERROR("400"),
    /**
     * 身份认证异常
     */
    AUTHENTICATE_ERROR("401"),
    /**
     * 权限验证异常
     */
    AUTHORIZATION_ERROR("405"),
    /**
     * 参数校验异常
     */
    PARAM_VALIDATE_ERROR("406"),
    /**
     * 预期外异常
     */
    UNEXPECTED_ERROR("500");

    /**
     * 响应状态码
     */
    private String code;

    ResponseCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
