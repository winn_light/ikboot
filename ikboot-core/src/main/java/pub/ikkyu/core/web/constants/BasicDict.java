package pub.ikkyu.core.web.constants;

/**
 * 全局字典
 *
 * @author ikkyu
 */
public interface BasicDict {

    /**
     * 数据状态
     */
    interface DataStatus {
        /**
         * 启用
         */
        String ENABLE = "1";
        /**
         * 停用
         */
        String DISABLE = "0";
    }

    /**
     * 排序类型
     */
    interface SortType {
        /**
         * 升序
         */
        String ASC = "asc";
        /**
         * 降序
         */
        String DESC = "desc";
    }

    /**
     * 是否项字典
     */
    interface Whether {
        /**
         * 是
         */
        String YES = "1";
        /**
         * 否
         */
        String NO = "0";
    }

}
