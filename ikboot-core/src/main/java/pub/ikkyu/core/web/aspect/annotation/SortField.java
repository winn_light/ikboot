package pub.ikkyu.core.web.aspect.annotation;

import java.lang.annotation.*;

/**
 * 排序字段注解, 用于处理 实体类字段名称>数据库字段名称 关系不为 驼峰命名>下划线分割 的情况
 *
 * @author ikkyu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface SortField {

    String value();

}
