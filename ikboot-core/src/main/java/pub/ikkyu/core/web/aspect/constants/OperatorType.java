package pub.ikkyu.core.web.aspect.constants;

/**
 * 操作人属性枚举
 *
 * @author ikkyu
 */
public enum OperatorType {

    /**
     * 创建人
     */
    CREATOR("createBy", "createTime"),
    /**
     * 修改人
     */
    MODIFIER("updateBy", "updateTime"),
    /**
     * 操作人
     */
    OPERATOR("operateBy", "operateTime");

    /**
     * 操作人属性名
     */
    private String operatorProp;

    /**
     * 操作时间属性名
     */
    private String operatedTimeProp;

    OperatorType(String operatorProp, String operatedTimeProp) {
        this.operatorProp = operatorProp;
        this.operatedTimeProp = operatedTimeProp;
    }

    public String getOperatorProp() {
        return operatorProp;
    }

    public String getOperatedTimeProp() {
        return operatedTimeProp;
    }

}
