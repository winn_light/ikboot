package pub.ikkyu.security.utils;

import pub.ikkyu.core.utils.StrUtil;

import java.util.UUID;

/**
 * token工具
 *
 * @author ikkyu
 */
public class TokenUtil {

    /**
     * 生成token
     */
    public static String generateToken() {
        return UUID.randomUUID().toString().replaceAll(StrUtil.UUID_SEPARATOR, StrUtil.EMPTY);
    }

}
