package pub.ikkyu.security.web;

import com.alibaba.fastjson.JSON;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import pub.ikkyu.core.utils.HttpUtil;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.entity.AjaxResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ajax认证失败处理
 *
 * @author ikkyu
 */
public class AjaxAuthenticationFailureFailedHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        // 针对不同异常展示不同的提示信息
        String message;
        if (exception instanceof DisabledException) {
            message = "账户已被禁用, 请联系管理员解锁!";
        } else {
            message = "用户名或密码错误, 请核对后重试!";
        }

        AjaxResult<Void> result = AjaxResult.error(ResponseCode.AUTHENTICATE_ERROR.getCode(), message, null);

        HttpUtil.response(response, JSON.toJSONString(result));
    }

}
