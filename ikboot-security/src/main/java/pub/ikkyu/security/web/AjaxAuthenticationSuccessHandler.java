package pub.ikkyu.security.web;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;
import pub.ikkyu.core.utils.HttpUtil;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.security.utils.TokenUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ajax认证成功处理
 *
 * @author ikkyu
 */
public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private CacheHelper cacheHelper;

    private SecurityProperties securityProperties;

    public AjaxAuthenticationSuccessHandler(CacheHelper cacheHelper, SecurityProperties securityProperties) {
        this.cacheHelper = cacheHelper;
        this.securityProperties = securityProperties;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String token = TokenUtil.generateToken();

        cacheHelper.put(securityProperties.getTokenCacheKey(), ((UserDetails) authentication.getPrincipal()).getUsername(), authentication.getPrincipal());
        cacheHelper.put(securityProperties.getTokenCacheKey(), token, authentication.getPrincipal());

        AjaxResult<String> result = AjaxResult.success(token);

        HttpUtil.response(response, JSON.toJSONString(result));
    }

}
