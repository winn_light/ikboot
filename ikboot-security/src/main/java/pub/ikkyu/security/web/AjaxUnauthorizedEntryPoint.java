package pub.ikkyu.security.web;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import pub.ikkyu.core.utils.HttpUtil;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.entity.AjaxResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ajax未认证异常处理
 *
 * @author ikkyu
 */
public class AjaxUnauthorizedEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) {
        AjaxResult<Void> result = AjaxResult.error(ResponseCode.AUTHENTICATE_ERROR.getCode(), "您的身份信息已失效, 请重新登陆!", null);

        HttpUtil.response(response, JSON.toJSONString(result));
    }

}
