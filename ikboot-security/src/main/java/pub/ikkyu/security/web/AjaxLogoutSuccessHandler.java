package pub.ikkyu.security.web;

import com.alibaba.fastjson.JSON;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;
import pub.ikkyu.core.utils.HttpUtil;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.security.entity.DefaultUserDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ajax登出成功处理
 *
 * @author ikkyu
 */
public class AjaxLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    private CacheHelper cacheHelper;

    private SecurityProperties securityProperties;

    public AjaxLogoutSuccessHandler(CacheHelper cacheHelper, SecurityProperties securityProperties) {
        this.cacheHelper = cacheHelper;
        this.securityProperties = securityProperties;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        AjaxResult<Void> result = AjaxResult.success();

        String token = request.getHeader(securityProperties.getTokenHeaderKey());

        DefaultUserDetails userDetails = (DefaultUserDetails) cacheHelper.get(securityProperties.getTokenCacheKey(), token);

        if (userDetails == null) {
            HttpUtil.response(response, JSON.toJSONString(result));
        } else {
            cacheHelper.remove(securityProperties.getTokenCacheKey(), userDetails.getUsername());
            cacheHelper.remove(securityProperties.getTokenCacheKey(), token);
        }

        HttpUtil.response(response, JSON.toJSONString(result));
    }

}
