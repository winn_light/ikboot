package pub.ikkyu.security.web;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.util.CollectionUtils;
import pub.ikkyu.core.security.SecurityProperties;
import pub.ikkyu.core.utils.LogUtil;

import java.util.Collection;

/**
 * 基于uri的权限验证实现
 *
 * @author ikkyu
 */
public class UriAccessDecisionVoter implements AccessDecisionVoter {

    private static final int ACCESS_DENIED = -1;

    private static final int ACCESS_SUCCEED = 1;

    private SecurityProperties securityProperties;

    public UriAccessDecisionVoter(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Override
    public int vote(Authentication authentication, Object object, Collection attributes) {
        if (authentication == null) {
            return ACCESS_DENIED;
        }

        String requestUri = ((FilterInvocation) object).getRequest().getRequestURI();

        // 认证后无需权限访问的API接口直接通过
        if (securityProperties != null && !CollectionUtils.isEmpty(securityProperties.getAuthenticatedApiList())) {
            if (securityProperties.getAuthenticatedApiList().contains(requestUri)) {
                return ACCESS_SUCCEED;
            }
        }

        try {
            Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) authentication.getAuthorities();
            if (CollectionUtils.isEmpty(authorities)) {
                return ACCESS_DENIED;
            }

            SimpleGrantedAuthority requiredAuthority = new SimpleGrantedAuthority(requestUri);

            if (authorities.contains(requiredAuthority)) {
                return ACCESS_SUCCEED;
            }

            return ACCESS_DENIED;
        } catch (Throwable ex) {
            LogUtil.error("URI鉴权失败: ", ex);
            return ACCESS_DENIED;
        }
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class aClass) {
        return true;
    }
}
