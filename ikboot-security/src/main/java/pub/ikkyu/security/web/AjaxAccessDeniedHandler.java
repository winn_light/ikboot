package pub.ikkyu.security.web;

import com.alibaba.fastjson.JSON;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import pub.ikkyu.core.utils.HttpUtil;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.entity.AjaxResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ajax权限验证异常处理
 *
 * @author ikkyu
 */
public class AjaxAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) throws IOException, ServletException {
        LogUtil.warn("无权限访问, URI: {}", request.getRequestURI());

        AjaxResult<Void> result = AjaxResult.error(ResponseCode.AUTHORIZATION_ERROR.getCode(), "抱歉, 您没有访问权限!", null);

        HttpUtil.response(response, JSON.toJSONString(result));
    }

}
