package pub.ikkyu.security.service;

import pub.ikkyu.security.entity.DefaultUserDetails;

/**
 * 认证授权管理 - 服务
 *
 * @author ikkyu
 */
public interface ICertifiedService {

    /**
     * 按用户名查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    DefaultUserDetails queryUserByUsername(String username);

    /**
     * 查询用户详细信息(含权限信息)
     *
     * @return 用户信息
     */
    DefaultUserDetails queryUserInfo();

    /**
     * 更新用户信息
     *
     * @param userDetails 用户信息
     */
    void updateUserInfo(DefaultUserDetails userDetails);
}
