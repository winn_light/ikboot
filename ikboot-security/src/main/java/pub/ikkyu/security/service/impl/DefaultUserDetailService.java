package pub.ikkyu.security.service.impl;

import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.security.service.ICertifiedService;

/**
 * 默认用户获取服务
 *
 * @author ikkyu
 */
public class DefaultUserDetailService implements UserDetailsService {

    private ICertifiedService certifiedService;

    public DefaultUserDetailService(ICertifiedService certifiedService) {
        this.certifiedService = certifiedService;
    }

    /**
     * 按用户名加载用户数据
     * {@link UsernameNotFoundException}异常会被{@link AbstractUserDetailsAuthenticationProvider}吃掉, 所以这里没能正常获取到用户时, 记录警告日志
     *
     * @param username 用户名
     * @return 用户数据
     * @throws UsernameNotFoundException 用户名未找到异常
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = certifiedService.queryUserByUsername(username);

        if (user == null) {
            String errorMessage = String.format("用户名不存在, username: %s", username);
            LogUtil.warn(errorMessage);
            throw new UsernameNotFoundException(errorMessage);
        }

        return user;
    }

}
