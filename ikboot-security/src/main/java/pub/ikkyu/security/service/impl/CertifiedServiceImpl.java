package pub.ikkyu.security.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;
import pub.ikkyu.core.web.constants.ResponseCode;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.platform.entity.User;
import pub.ikkyu.platform.service.IUserService;
import pub.ikkyu.security.entity.DefaultUserDetails;
import pub.ikkyu.security.service.ICertifiedService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 认证授权管理
 *
 * @author ikkyu
 */
@Service
public class CertifiedServiceImpl implements ICertifiedService {

    @Autowired
    private IUserService userService;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private CacheHelper cacheHelper;

    @Override
    public DefaultUserDetails queryUserByUsername(String username) {
        User user = userService.queryUserByUsername(username);

        DefaultUserDetails userDetails = new DefaultUserDetails();
        BeanUtils.copyProperties(user, userDetails);

        return userDetails;
    }

    @Override
    public DefaultUserDetails queryUserInfo() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getHeader(securityProperties.getTokenHeaderKey());

        DefaultUserDetails userDetails = (DefaultUserDetails) cacheHelper.get(securityProperties.getTokenCacheKey(), token);
        if (userDetails == null) {
            throw new ServiceException(ResponseCode.AUTHENTICATE_ERROR, "您的身份信息已失效, 请重新登陆!");
        }

        User condition = new User();
        BeanUtils.copyProperties(userDetails, condition);

        User authorityInfo = userService.queryAuthorityInfo(condition);
        userDetails.setMenus(authorityInfo.getMenus());
        userDetails.setFunctions(authorityInfo.getFunctions());
        // 刷新用户已授权的API接口
        List<SimpleGrantedAuthority> authorities = new ArrayList<>(authorityInfo.getApis().size());
        authorityInfo.getApis().forEach(item -> authorities.add(new SimpleGrantedAuthority(item)));
        userDetails.setAuthorities(authorities);

        cacheHelper.put(securityProperties.getTokenCacheKey(), userDetails.getUsername(), userDetails);
        cacheHelper.put(securityProperties.getTokenCacheKey(), token, userDetails);

        return userDetails;
    }

    @Override
    public void updateUserInfo(DefaultUserDetails userDetails) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String token = request.getHeader(securityProperties.getTokenHeaderKey());

        DefaultUserDetails cachedUserDetails = (DefaultUserDetails) cacheHelper.get(securityProperties.getTokenCacheKey(), token);
        if (cachedUserDetails == null) {
            throw new ServiceException(ResponseCode.AUTHENTICATE_ERROR, "您的身份信息已失效, 请重新登陆!");
        }

        User user = new User();
        BeanUtils.copyProperties(userDetails, user);

        userService.updateUserByUsername(user);

        // 未更新密码, 刷新用户缓存信息
        if (StringUtils.isEmpty(user.getPassword())) {
            cachedUserDetails.setPhonenumber(userDetails.getPhonenumber());
            cachedUserDetails.setEmail(userDetails.getEmail());
            cachedUserDetails.setAvatar(userDetails.getAvatar());
            cachedUserDetails.setFullName(userDetails.getFullName());

            cacheHelper.put(securityProperties.getTokenCacheKey(), token, cachedUserDetails);
            cacheHelper.put(securityProperties.getTokenCacheKey(), cachedUserDetails.getUsername(), cachedUserDetails);
            // 更新了密码, 强制下线
        } else {
            cacheHelper.remove(securityProperties.getTokenCacheKey(), cachedUserDetails.getUsername());
            cacheHelper.remove(securityProperties.getTokenCacheKey(), token);
        }
    }
}
