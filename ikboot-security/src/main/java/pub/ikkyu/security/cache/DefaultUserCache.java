package pub.ikkyu.security.cache;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;

/**
 * 默认用户信息缓存实现
 *
 * @author ikkyu
 */
public class DefaultUserCache implements UserCache {

    private SecurityProperties securityProperties;

    private CacheHelper cacheHelper;

    public DefaultUserCache(SecurityProperties securityProperties, CacheHelper cacheHelper) {
        this.securityProperties = securityProperties;
        this.cacheHelper = cacheHelper;
    }

    @Override
    public UserDetails getUserFromCache(String username) {
        return (UserDetails) cacheHelper.get(securityProperties.getTokenCacheKey(), username);
    }

    @Override
    public void putUserInCache(UserDetails userDetails) {
        cacheHelper.put(securityProperties.getTokenCacheKey(), userDetails.getUsername(), userDetails);
    }

    @Override
    public void removeUserFromCache(String username) {
        cacheHelper.remove(securityProperties.getTokenCacheKey(), username);
    }
}
