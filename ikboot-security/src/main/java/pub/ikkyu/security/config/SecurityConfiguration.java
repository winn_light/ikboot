package pub.ikkyu.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.authentication.CachingUserDetailsService;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.CollectionUtils;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;
import pub.ikkyu.core.security.crypto.BCryptPasswordEncoder;
import pub.ikkyu.security.cache.DefaultUserCache;
import pub.ikkyu.security.filter.TokenAuthenticationFilter;
import pub.ikkyu.security.service.ICertifiedService;
import pub.ikkyu.security.service.impl.DefaultUserDetailService;
import pub.ikkyu.security.web.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Spring Security安全认证配置
 * 是否开启取决于${ik-app.security.enable}属性值, 默认给true是为了防止生产环境缺失该参数导致应用无安全控制的问题
 *
 * @author ikkyu
 */
@ConditionalOnProperty(value = "ik-app.security.enable", matchIfMissing = true)
@Import(SecurityAutoConfiguration.class)
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    public SecurityProperties securityProperties;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CacheHelper cacheHelper;

    @Autowired
    private ICertifiedService certifiedService;

    @Override
    public void configure(WebSecurity web) throws Exception {
        // 放行静态资源
        web.ignoring().antMatchers("/avatar/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 禁用CSRF
        http.csrf().disable();

        // 关闭session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        String loginUri = securityProperties.getLoginUri();
        String logoutUri = securityProperties.getLogoutUri();

        // 注册登陆接口
        http.formLogin().loginProcessingUrl(loginUri).usernameParameter("username").passwordParameter("password");

        // 注册认证成功&认证失败处理类
        http.formLogin().successHandler(new AjaxAuthenticationSuccessHandler(cacheHelper, securityProperties)).failureHandler(new AjaxAuthenticationFailureFailedHandler());

        // 注册登出接口
        http.logout().logoutUrl(logoutUri).logoutSuccessHandler(new AjaxLogoutSuccessHandler(cacheHelper, securityProperties));

        // 注册请求白名单
        if (CollectionUtils.isEmpty(securityProperties.getWhiteApiList())) {
            http.authorizeRequests().antMatchers(loginUri, logoutUri).permitAll();
        } else {
            securityProperties.getWhiteApiList().add(loginUri);
            securityProperties.getWhiteApiList().add(logoutUri);
            http.authorizeRequests().antMatchers(securityProperties.getWhiteApiList().toArray(new String[]{})).permitAll();
        }

        // 任意请求都需要经过认证
        http.authorizeRequests().anyRequest().authenticated();

        // 注册未认证访问异常处理类
        http.exceptionHandling().authenticationEntryPoint(new AjaxUnauthorizedEntryPoint());

        // 注册鉴权处理类
        http.authorizeRequests().accessDecisionManager(accessDecisionManager());

        // 注册权限验证异常处理类
        http.exceptionHandling().accessDeniedHandler(new AjaxAccessDeniedHandler());

        // 注册token过滤器
        http.addFilterBefore(new TokenAuthenticationFilter(securityProperties, cacheHelper), UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * 用户信息获取服务配置
     */
    @Bean
    public UserDetailsService cachingUserDetailsService() {
        CachingUserDetailsService cachingUserDetailsService = new CachingUserDetailsService(new DefaultUserDetailService(certifiedService));
        cachingUserDetailsService.setUserCache(new DefaultUserCache(securityProperties, cacheHelper));

        return cachingUserDetailsService;
    }

    /**
     * 密码加密配置
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return bCryptPasswordEncoder.encode(charSequence);
            }

            @Override
            public boolean matches(CharSequence charSequence, String password) {
                return bCryptPasswordEncoder.matches(charSequence, password);
            }
        };
    }

    /**
     * 鉴权管理器配置
     */
    @Bean
    public AccessDecisionManager accessDecisionManager() {
        List<AccessDecisionVoter<?>> decisionVoters = new ArrayList<>();
        decisionVoters.add(new UriAccessDecisionVoter(securityProperties));

        return new UnanimousBased(decisionVoters);
    }


}
