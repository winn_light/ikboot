package pub.ikkyu.security.entity;

import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pub.ikkyu.core.json.annotation.Desensitization;
import pub.ikkyu.core.json.constants.DesensitizationType;
import pub.ikkyu.core.web.constants.BasicDict;

import java.util.*;

/**
 * label: 登陆用户信息
 *
 * @author ikkyu
 */
public class DefaultUserDetails implements UserDetails {

    private static final long serialVersionUID = -7721002754746650012L;

    /**
     * label: 用户名
     */
    private String username;

    /**
     * label: 姓名
     */
    private String fullName;

    /**
     * label: 用户密码
     */
    @JSONField(serialize = false)
    @Desensitization(DesensitizationType.PASSWORD)
    private String password;

    /**
     * label: 所属机构
     */
    private String orgName;

    /**
     * label: 用户状态, 1-启用, 0-停用
     */
    @JSONField(serialize = false)
    private String status;

    /**
     * label: 是否超管用户
     */
    @JSONField(serialize = false)
    private String admin;

    /**
     * label: 手机号
     */
    private String phonenumber;

    /**
     * label: 邮箱
     */
    private String email;

    /**
     * label: 头像路径
     */
    private String avatar;

    /**
     * label: 可访问菜单编码集合
     */
    private Set<String> menus;

    /**
     * label: 可访问功能编码集合
     */
    private Map<String, Boolean> functions;

    /**
     * label: Spring Security 鉴权使用
     */
    @JSONField(serialize = false)
    private List<SimpleGrantedAuthority> authorities;

    public DefaultUserDetails() {

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<SimpleGrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    @JSONField(serialize = false)
    public boolean isEnabled() {
        return BasicDict.DataStatus.ENABLE.equals(this.status);
    }

    /**
     * 账户不过期
     */
    @Override
    @JSONField(serialize = false)
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 不锁定
     */
    @Override
    @JSONField(serialize = false)
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 凭证不过期
     */
    @Override
    @JSONField(serialize = false)
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Set<String> getMenus() {
        return menus;
    }

    public void setMenus(Set<String> menus) {
        this.menus = menus;
    }

    public Map<String, Boolean> getFunctions() {
        return functions;
    }

    public void setFunctions(Map<String, Boolean> functions) {
        this.functions = functions;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DefaultUserDetails.class.getSimpleName() + "[", "]")
                .add("username='" + username + "'")
                .add("fullName='" + fullName + "'")
                .add("password='" + password + "'")
                .add("orgName='" + orgName + "'")
                .add("status='" + status + "'")
                .add("admin='" + admin + "'")
                .add("phonenumber='" + phonenumber + "'")
                .add("email='" + email + "'")
                .add("avatar='" + avatar + "'")
                .add("menus=" + menus)
                .add("functions=" + functions)
                .add("authorities=" + authorities)
                .toString();
    }
}
