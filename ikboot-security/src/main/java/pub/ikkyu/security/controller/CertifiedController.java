package pub.ikkyu.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.security.entity.DefaultUserDetails;
import pub.ikkyu.security.service.ICertifiedService;

/**
 * label: 认证授权管理
 *
 * @author ikkyu
 */
@RequestMapping("/security")
@RestController
public class CertifiedController {

    @Autowired
    private ICertifiedService certifiedService;

    /**
     * label: 查询当前登陆人信息
     */
    @GetMapping("/userinfo/query")
    public AjaxResult<DefaultUserDetails> queryUserInfo() {
        return AjaxResult.success(certifiedService.queryUserInfo());
    }

    /**
     * label: 更新当前登陆人信息
     */
    @PostMapping("/userinfo/update")
    public AjaxResult<Void> updateUserInfo(DefaultUserDetails userDetails) {
        certifiedService.updateUserInfo(userDetails);

        return AjaxResult.success();
    }

}
