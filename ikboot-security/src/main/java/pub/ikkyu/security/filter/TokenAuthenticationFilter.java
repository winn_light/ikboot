package pub.ikkyu.security.filter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.security.SecurityProperties;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token鉴权过滤器
 *
 * @author ikkyu
 */
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    /**
     * 缓存组件
     */
    private CacheHelper cacheHelper;

    private SecurityProperties securityProperties;

    public TokenAuthenticationFilter(SecurityProperties securityProperties, CacheHelper cacheHelper) {
        this.securityProperties = securityProperties;
        this.cacheHelper = cacheHelper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader(securityProperties.getTokenHeaderKey());

        if (StringUtils.isEmpty(token)) {
            filterChain.doFilter(request, response);
            return;
        }

        UserDetails userDetails = (UserDetails) cacheHelper.get(securityProperties.getTokenCacheKey(), token);
        if (userDetails == null) {
            filterChain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

}
