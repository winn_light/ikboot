#!/bin/sh
RESOURCE_NAME=ikboot.jar

# 获取脚本所在路径
CURRENT_DIR_PATH=$(readlink -f "$(dirname "$0")")

# 判断应用是否已启动
tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
    echo 'App is running.'
else
    echo 'App is not running.'
fi

# 移除产生的临时文件
rm -f tpid

# 启动
nohup java -jar $CURRENT_DIR_PATH/../$RESOURCE_NAME >/dev/null 2>&1 &

echo 'App is starting.'