#!/bin/sh
RESOURCE_NAME=ikboot.jar

tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
  echo 'App process killed success.'
kill -9 $tpid
else
  echo 'App is not running.'
fi

# 移除产生的临时文件
rm -f tpid