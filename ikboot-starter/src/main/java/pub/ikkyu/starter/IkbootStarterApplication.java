package pub.ikkyu.starter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.cache.annotation.EnableCaching;

import java.io.File;

/**
 * <h2>SpringBoot 应用启动类</h2>
 * <ol>
 *     <li>
 *         排除{@link SecurityAutoConfiguration}是因为在应用开发过程中难以保持固定token, 导致调试难度大,
 *         故使用ik-app.security.enable控制安全模块是否启动, 详情参见: {@link pub.ikkyu.security.config.SecurityConfiguration}
 *     </li>
 *     <li>
 *         数据缓存请统一使用 {@link pub.ikkyu.core.cache.CacheHelper} 组件,
 *         该组件支持EHCache、Redis两种缓存，请根据实际需要在starter模块中引入相应jar包
 *         优先级: redis > ehcache, 如二者都不引入则禁止使用CacheHelper组件
 *     </li>
 *     <li>
 *         应用产生的日志文件路径通过 ikapp.logging.file-path 属性配置, 默认为当前应用部署目录, 请确保应用对当前部署目录有读写权限
 *     </li>
 *     <li>
 *         应用开发过程中可开启debug模式, 直接将后端错误信息响应到前端, 以便开发/联调过程中根据错误提示快速定位问题
 *     </li>
 * </ol>
 *
 * @author ikkyu
 */
@EnableCaching
@SpringBootApplication(scanBasePackages = "pub.ikkyu", exclude = {SecurityAutoConfiguration.class})
@MapperScan("pub.ikkyu.**.mapper")
public class IkbootStarterApplication {

    public static void main(String[] args) {
        ApplicationHome applicationHome = new ApplicationHome();
        File jarFile = applicationHome.getDir();
        System.setProperty("ikapp.logging.file-path", jarFile.toString());

        SpringApplication.run(IkbootStarterApplication.class, args);
    }

}