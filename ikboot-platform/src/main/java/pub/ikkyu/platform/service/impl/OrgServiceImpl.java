package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BusinessConstant;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.core.web.utils.TreeUtil;
import pub.ikkyu.platform.entity.Org;
import pub.ikkyu.platform.mapper.IOrgMapper;
import pub.ikkyu.platform.service.IOrgService;

import java.util.ArrayList;
import java.util.List;

/**
 * label: 机构管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class OrgServiceImpl implements IOrgService {

    @Autowired
    private IOrgMapper orgMapper;

    @OperateInfoSet(OperatorType.CREATOR)
    @Override
    public void insert(Org org) {
        if (org.getParentId() == null) {
            org.setParentId(BusinessConstant.TREE_ROOT_ID);
        }

        int sameLevelMultipleNameCount = orgMapper.countSameLevelMultipleName(org);
        if (sameLevelMultipleNameCount > 0) {
            throw new ServiceException("同级机构名称不允许重复!");
        }

        int rows = orgMapper.insert(org);
        if (rows == 0) {
            throw new ServiceException("机构写入失败, 请稍后重试!");
        }
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void update(Org org) {
        int sameLevelMultipleNameCount = orgMapper.countSameLevelMultipleName(org);
        if (sameLevelMultipleNameCount > 0) {
            throw new ServiceException("同级机构名称不允许重复!");
        }

        int rows = orgMapper.update(org);
        if (rows == 0) {
            throw new ServiceException("机构可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public void delete(Org org) {
        int sonOrgCount = orgMapper.countSonOrg(org);
        if (sonOrgCount > 0) {
            throw new ServiceException("请先删除当前机构的所有子机构后重试!");
        }

        int relatedUserCount = orgMapper.countRelatedUser(org);
        if (relatedUserCount > 0) {
            throw new ServiceException("机构下已有人员, 请转移人员后重试!");
        }

        int rows = orgMapper.delete(org);
        if (rows == 0) {
            throw new ServiceException("机构可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public List<Org> listTree(Page page, Org condition) {
        PageHelper.orderBy(page.getSortStr());
        List<Org> orgList = orgMapper.list(condition);

        orgList.forEach(item -> {
            item.setChildren(new ArrayList<>());
        });
        return TreeUtil.castToTreeList(orgList);
    }

}