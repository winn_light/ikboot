package pub.ikkyu.platform.service;

import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.Role;

import java.util.List;

/**
 * label: 角色管理 - 服务
 *
 * @author iktools
 */
public interface IRoleService {

    /**
     * 新增角色
     *
     * @param role 角色数据
     */
    void insert(Role role);

    /**
     * 查询角色
     *
     * @param condition 查询条件
     * @return 角色数据
     */
    Role query(Role condition);

    /**
     * 修改角色
     *
     * @param role 角色数据
     */
    void update(Role role);

    /**
     * 修改角色状态
     *
     * @param role 角色数据
     */
    void updateStatus(Role role);

    /**
     * 删除角色
     *
     * @param role 角色数据
     */
    void delete(Role role);

    /**
     * 分页查询角色
     *
     * @param page      分页排序条件
     * @param condition 查询条件
     * @return 角色分页结果集
     */
    PageResult<Role> list(Page page, Role condition);

    /**
     * 查询启用的所有角色
     *
     * @return 启用角色列表
     */
    List<Role> enabledList();
}