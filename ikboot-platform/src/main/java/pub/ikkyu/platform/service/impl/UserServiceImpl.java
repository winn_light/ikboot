package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import pub.ikkyu.core.security.crypto.BCryptPasswordEncoder;
import pub.ikkyu.core.utils.FileUtil;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BasicDict;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.platform.constants.PlatformDict;
import pub.ikkyu.platform.entity.Menu;
import pub.ikkyu.platform.entity.User;
import pub.ikkyu.platform.mapper.IUserMapper;
import pub.ikkyu.platform.service.IUserService;

import java.util.*;

/**
 * label: 用户管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class UserServiceImpl implements IUserService {

    /**
     * 用户头像上传模块编码
     */
    private static final String AVATAR_UPLOAD_MODULE_CODE = "avatar";

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private IUserMapper userMapper;

    @OperateInfoSet(OperatorType.CREATOR)
    @Override
    public void insert(User user) {
        int multipleUsernameCount = userMapper.countMultipleUsername(user);
        if (multipleUsernameCount > 0) {
            throw new ServiceException("用户名不允许重复, 请修改后重试!");
        }

        user.setStatus(BasicDict.DataStatus.ENABLE);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        try {
            int rows = userMapper.insert(user);
            if (rows == 0) {
                throw new ServiceException("用户写入失败, 请稍后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("新增用户失败: ", ex);
            throw new ServiceException("用户名不允许重复, 请修改后重试!");
        }

        if (!CollectionUtils.isEmpty(user.getRoleIds())) {
            int userRoleRelationRows = userMapper.insertUserRoleRelation(user);
            if (userRoleRelationRows != user.getRoleIds().size()) {
                throw new ServiceException("用户角色关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @Override
    public User query(User condition) {
        User user = userMapper.query(condition);
        if (user == null) {
            throw new ServiceException("用户可能已被删除, 请刷新后重试!");
        }

        user.setRoleIds(userMapper.queryRoleIds(condition));

        return user;
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void update(User user) {
        int multipleUsernameCount = userMapper.countMultipleUsername(user);
        if (multipleUsernameCount > 0) {
            throw new ServiceException("用户名不允许重复, 请修改后重试!");
        }

        if (!StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        try {
            int rows = userMapper.update(user);
            if (rows == 0) {
                throw new ServiceException("用户可能已被删除, 请刷新后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("编辑用户失败: ", ex);
            throw new ServiceException("用户名不允许重复, 请修改后重试!");
        }

        userMapper.deleteUserRoleRelation(user);
        if (!CollectionUtils.isEmpty(user.getRoleIds())) {
            int userRoleRelationRows = userMapper.insertUserRoleRelation(user);
            if (userRoleRelationRows != user.getRoleIds().size()) {
                throw new ServiceException("用户角色关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void updateStatus(User user) {
        int rows = userMapper.updateStatus(user);
        if (rows == 0) {
            throw new ServiceException("用户可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public PageResult<User> list(Page page, User condition) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getSortStr());
        PageInfo<User> pageResult = new PageInfo<>(userMapper.list(condition));
        return PageResult.success(pageResult.getTotal(), pageResult.getList());
    }

    @Override
    public User queryUserByUsername(String username) {
        if (StringUtils.isEmpty(username)) {
            return null;
        }

        User condition = new User();
        condition.setUsername(username);

        return userMapper.queryByUsername(condition);
    }

    @Override
    public User queryAuthorityInfo(User condition) {
        User user = new User();

        // 获取权限信息
        List<Menu> authorities = userMapper.queryAuthorityByUsername(condition);
        // 用户功能权限ID&用户菜单权限&功能权限集合
        Set<String> functionIds = new HashSet<>();
        Set<String> menus = new HashSet<>();
        Map<String, Boolean> functions = new HashMap<>();
        // 转换为前台所需的数据
        for (Menu menu : authorities) {
            if (PlatformDict.MenuType.FUNCTION.equals(menu.getType())) {
                functions.put(menu.getCode(), Boolean.TRUE);
                functionIds.add(menu.getId());
            } else {
                menus.add(menu.getCode());
            }
        }
        user.setMenus(menus);
        user.setFunctions(functions);

        if (!CollectionUtils.isEmpty(functionIds)) {
            // 获取可访问API资源集合
            user.setApis(userMapper.queryApiByFunctionIds(functionIds));
        } else {
            user.setApis(new HashSet<>(0));
        }

        return user;
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void updateUserByUsername(User user) {
        if (!StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        int rows = userMapper.updateByUsername(user);
        if (rows == 0) {
            throw new ServiceException("用户不存在, 请刷新后重试!");
        }
    }

    @Override
    public String uploadAvatar(MultipartFile file) {
        return FileUtil.uploadFile(AVATAR_UPLOAD_MODULE_CODE, file);
    }

    // TODO#ikkyu 新增用户密码重置接口

}