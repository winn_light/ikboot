package pub.ikkyu.platform.service;

import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.Dict;
import pub.ikkyu.platform.entity.DictItem;

import java.util.List;
import java.util.Map;

/**
 * label: 字典管理 - 服务
 *
 * @author iktools
 */
public interface IDictService {

    /**
     * 新增字典
     *
     * @param dict 字典数据
     */
    void insert(Dict dict);

    /**
     * 修改字典
     *
     * @param dict 字典数据
     */
    void update(Dict dict);

    /**
     * 修改字典状态
     *
     * @param dict 字典数据
     */
    void updateStatus(Dict dict);

    /**
     * 删除字典
     *
     * @param dict 字典数据
     */
    void delete(Dict dict);

    /**
     * 分页查询字典
     *
     * @param page      分页排序条件
     * @param condition 查询条件
     * @return 字典分页结果集
     */
    PageResult<Dict> list(Page page, Dict condition);

    /**
     * 按字典编码集合查询数据字典
     *
     * @param dictCodes
     * @return
     */
    Map<String, List<DictItem>> listDataDictByCodes(String[] dictCodes);
}