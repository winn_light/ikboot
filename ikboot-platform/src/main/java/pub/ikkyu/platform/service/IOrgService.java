package pub.ikkyu.platform.service;

import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.platform.entity.Org;

import java.util.List;

/**
 * label: 机构管理 - 服务
 *
 * @author iktools
 */
public interface IOrgService {

    /**
     * 新增机构
     *
     * @param org 机构数据
     */
    void insert(Org org);

    /**
     * 修改机构
     *
     * @param org 机构数据
     */
    void update(Org org);

    /**
     * 删除机构
     *
     * @param org 机构数据
     */
    void delete(Org org);

    /**
     * 条件查询机构树
     *
     * @param page      排序条件
     * @param condition 查询条件
     * @return 机构树
     */
    List<Org> listTree(Page page, Org condition);
}