package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BasicDict;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.platform.entity.Dict;
import pub.ikkyu.platform.entity.DictItem;
import pub.ikkyu.platform.mapper.IDictItemMapper;
import pub.ikkyu.platform.mapper.IDictMapper;
import pub.ikkyu.platform.service.IDictService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pub.ikkyu.platform.constants.CacheAttribute.DATA_DICT_CACHE_KEY;

/**
 * label: 字典管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class DictServiceImpl implements IDictService {

    @Autowired
    private IDictMapper dictMapper;

    @Autowired
    private IDictItemMapper dictItemMapper;

    @Autowired
    private CacheHelper cacheHelper;

    @OperateInfoSet(value = OperatorType.CREATOR)
    @Override
    public void insert(Dict dict) {
        int multipleCodeCount = dictMapper.countMultipleCode(dict);
        if (multipleCodeCount > 0) {
            throw new ServiceException("字典编码重复, 请修改后重试!");
        }

        dict.setStatus(BasicDict.DataStatus.ENABLE);
        try {
            int rows = dictMapper.insert(dict);
            if (rows == 0) {
                throw new ServiceException("字典写入失败, 请稍后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("新增字典失败: ", ex);
            throw new ServiceException("字典编码重复, 请修改后重试!");
        }
    }

    @OperateInfoSet(value = OperatorType.MODIFIER)
    @Override
    public void update(Dict dict) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dict.getCode());

        int multipleCodeCount = dictMapper.countMultipleCode(dict);
        if (multipleCodeCount > 0) {
            throw new ServiceException("字典编码重复, 请修改后重试!");
        }

        try {
            int rows = dictMapper.update(dict);
            if (rows == 0) {
                throw new ServiceException("字典可能已被删除, 请刷新后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("更新字典失败: ", ex);
            throw new ServiceException("字典编码重复, 请修改后重试!");
        }
    }

    @OperateInfoSet(value = OperatorType.MODIFIER)
    @Override
    public void updateStatus(Dict dict) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dict.getCode());

        int rows = dictMapper.updateStatus(dict);
        if (rows == 0) {
            throw new ServiceException("字典可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public void delete(Dict dict) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dict.getCode());

        int rows = dictMapper.delete(dict);
        if (rows == 0) {
            throw new ServiceException("字典可能已被删除, 请刷新后重试!");
        }

        DictItem deleteCondition = new DictItem();
        deleteCondition.setDictId(dict.getId());
        dictItemMapper.delete(deleteCondition);
    }

    @Override
    public PageResult<Dict> list(Page page, Dict condition) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getSortStr());
        PageInfo<Dict> pageResult = new PageInfo<>(dictMapper.list(condition));
        return PageResult.success(pageResult.getTotal(), pageResult.getList());
    }

    @Override
    public Map<String, List<DictItem>> listDataDictByCodes(String[] dictCodes) {
        List<String> dictCodeList = Lists.newArrayList(dictCodes);
        Map<String, List<DictItem>> dictMap = new HashMap<>(dictCodeList.size());

        // 读取缓存数据
        for (String dictCode : new ArrayList<>(dictCodeList)) {
            List<DictItem> dictItems = (List<DictItem>) cacheHelper.get(DATA_DICT_CACHE_KEY, dictCode);
            if (dictItems != null) {
                dictMap.put(dictCode, dictItems);
                dictCodeList.remove(dictCode);
            }
        }

        // 取无缓存的数据
        if (dictCodeList.size() > 0) {
            List<DictItem> dictItemList = dictItemMapper.listByDictCodes(dictCodeList);

            for (DictItem dictItem : dictItemList) {
                dictMap.putIfAbsent(dictItem.getDictCode(), new ArrayList<>());
                dictMap.get(dictItem.getDictCode()).add(dictItem);
            }

            for (String dictCode : dictMap.keySet()) {
                cacheHelper.put(DATA_DICT_CACHE_KEY, dictCode, dictMap.get(dictCode));
            }
        }

        return dictMap;
    }

}