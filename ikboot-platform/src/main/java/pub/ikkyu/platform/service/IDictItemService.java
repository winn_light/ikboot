package pub.ikkyu.platform.service;

import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.DictItem;

/**
 * label: 字典项管理 - 服务
 *
 * @author iktools
 */
public interface IDictItemService {

    /**
     * 新增字典项
     *
     * @param dictItem 字典项数据
     */
    void insert(DictItem dictItem);

    /**
     * 修改字典项
     *
     * @param dictItem 字典项数据
     */
    void update(DictItem dictItem);

    /**
     * 修改字典项状态
     *
     * @param dictItem 字典项数据
     */
    void updateStatus(DictItem dictItem);

    /**
     * 删除字典项
     *
     * @param dictItem 字典项数据
     */
    void delete(DictItem dictItem);

    /**
     * 分页查询字典项
     *
     * @param page      分页排序条件
     * @param condition 查询条件
     * @return 字典项分页结果集
     */
    PageResult<DictItem> list(Page page, DictItem condition);

}