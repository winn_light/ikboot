package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BasicDict;
import pub.ikkyu.core.web.constants.BusinessConstant;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.core.web.utils.TreeUtil;
import pub.ikkyu.platform.constants.PlatformDict;
import pub.ikkyu.platform.entity.Menu;
import pub.ikkyu.platform.mapper.IMenuMapper;
import pub.ikkyu.platform.service.IMenuService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * label: 菜单管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class MenuServiceImpl implements IMenuService {

    @Autowired
    private IMenuMapper menuMapper;

    @OperateInfoSet(OperatorType.CREATOR)
    @Override
    public void insert(Menu menu) {
        int multipleCodeCount = menuMapper.countMultipleCode(menu);
        if (multipleCodeCount > 0) {
            throw new ServiceException("菜单编码重复, 请修改后重试!");
        }

        menu.setStatus(BasicDict.DataStatus.ENABLE);
        if (menu.getParentId() == null) {
            menu.setParentId(BusinessConstant.TREE_ROOT_ID);
        }
        try {
            int rows = menuMapper.insert(menu);
            if (rows == 0) {
                throw new ServiceException("菜单写入失败, 请稍后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("新增菜单失败: ", ex);
            throw new ServiceException("菜单编码重复, 请修改后重试!");
        }

        if (!CollectionUtils.isEmpty(menu.getApis())) {
            int menuApiRelationRows = menuMapper.insertMenuApiRelation(menu);
            if (menuApiRelationRows != menu.getApis().size()) {
                throw new ServiceException("菜单API关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @Override
    public Menu query(Menu condition) {
        Menu menu = menuMapper.query(condition);
        if (menu == null) {
            throw new ServiceException("菜单可能已被删除, 请刷新后重试!");
        }

        if (PlatformDict.MenuType.FUNCTION.equals(menu.getType())) {
            menu.setApis(menuMapper.listMenuApi(menu));
        }

        return menu;
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void update(Menu menu) {
        int multipleCodeCount = menuMapper.countMultipleCode(menu);
        if (multipleCodeCount > 0) {
            throw new ServiceException("菜单编码重复, 请修改后重试!");
        }

        try {
            int rows = menuMapper.update(menu);
            if (rows == 0) {
                throw new ServiceException("菜单可能已被删除, 请刷新后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("更新菜单失败: ", ex);
            throw new ServiceException("菜单编码重复, 请修改后重试!");
        }

        if (!CollectionUtils.isEmpty(menu.getApis())) {
            menuMapper.deleteMenuApiRelation(menu);

            int menuApiRelationRows = menuMapper.insertMenuApiRelation(menu);
            if (menuApiRelationRows != menu.getApis().size()) {
                throw new ServiceException("菜单API关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void updateStatus(Menu menu) {
        Set<String> ids = getChildrenIds(menu.getId());

        int rows = menuMapper.updateStatus(ids, menu);
        if (rows != ids.size()) {
            throw new ServiceException("部分菜单状态变更失败, 请稍后后重试!");
        }
    }

    @Override
    public void delete(Menu menu) {
        int sonMenuCount = menuMapper.countSonMenu(menu);
        if (sonMenuCount > 0) {
            throw new ServiceException("请先删除当前菜单的所有子菜单后重试!");
        }

        int rows = menuMapper.delete(menu);
        if (rows == 0) {
            throw new ServiceException("菜单可能已被删除, 请刷新后重试!");
        }

        menuMapper.deleteRoleMenuRelation(menu);
        if (PlatformDict.MenuType.FUNCTION.equals(menu.getType())) {
            menuMapper.deleteMenuApiRelation(menu);
        }
    }

    @Override
    public List<Menu> listTree(Page page, Menu condition) {
        PageHelper.orderBy(page.getSortStr());
        List<Menu> menuList = menuMapper.list(condition);

        menuList.forEach(item -> {
            item.setChildren(new ArrayList<>());
        });
        return TreeUtil.castToTreeList(menuList);
    }

    private Set<String> getChildrenIds(String parentId) {
        Set<String> idList = new HashSet<>();
        idList.add(parentId);

        List<String> parentIds;
        List<String> childrenIds = null;
        do {
            if (childrenIds == null) {
                parentIds = Lists.newArrayList(parentId);
            } else {
                parentIds = childrenIds;
            }

            childrenIds = menuMapper.listChildrenIds(parentIds);
            idList.addAll(childrenIds);
        } while (childrenIds.size() > 0);

        return idList;
    }

}