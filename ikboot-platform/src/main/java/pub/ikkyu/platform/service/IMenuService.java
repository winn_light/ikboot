package pub.ikkyu.platform.service;

import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.platform.entity.Menu;

import java.util.List;

/**
 * label: 菜单管理 - 服务
 *
 * @author iktools
 */
public interface IMenuService {

    /**
     * 新增菜单
     *
     * @param menu 菜单数据
     */
    void insert(Menu menu);

    /**
     * 按ID查询菜单
     *
     * @param condition 查询条件
     * @return 菜单数据
     */
    Menu query(Menu condition);

    /**
     * 修改菜单
     *
     * @param menu 菜单数据
     */
    void update(Menu menu);

    /**
     * 修改菜单状态
     *
     * @param menu 菜单数据
     */
    void updateStatus(Menu menu);

    /**
     * 删除菜单
     *
     * @param menu 菜单数据
     */
    void delete(Menu menu);

    /**
     * 条件查询菜单树
     *
     * @param condition 查询条件
     * @return 菜单列表
     */
    List<Menu> listTree(Page page, Menu condition);
}