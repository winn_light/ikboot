package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import pub.ikkyu.core.utils.LogUtil;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BasicDict;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.platform.entity.Role;
import pub.ikkyu.platform.mapper.IRoleMapper;
import pub.ikkyu.platform.service.IRoleService;

import java.util.List;

/**
 * label: 角色管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleMapper roleMapper;

    @OperateInfoSet(OperatorType.CREATOR)
    @Override
    public void insert(Role role) {
        int multipleNameCount = roleMapper.countMultipleName(role);
        if (multipleNameCount > 0) {
            throw new ServiceException("角色名称不允许重复, 请修改后重试!");
        }

        role.setStatus(BasicDict.DataStatus.ENABLE);
        try {
            int rows = roleMapper.insert(role);
            if (rows == 0) {
                throw new ServiceException("角色写入失败, 请稍后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("新增角色失败: ", ex);
            throw new ServiceException("橘色名称重复, 请修改后重试!");
        }

        if (!CollectionUtils.isEmpty(role.getMenuIds())) {
            int roleMenuRelationRows = roleMapper.insertRoleMenuRelation(role);
            if (roleMenuRelationRows != role.getMenuIds().size()) {
                throw new ServiceException("角色菜单关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @Override
    public Role query(Role condition) {
        Role role = roleMapper.query(condition);
        if (role == null) {
            throw new ServiceException("角色可能已被删除, 请刷新后重试!");
        }
        role.setMenuIds(roleMapper.listRoleMenu(condition));

        return role;
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void update(Role role) {
        int multipleNameCount = roleMapper.countMultipleName(role);
        if (multipleNameCount > 0) {
            throw new ServiceException("角色名称不允许重复, 请修改后重试!");
        }

        try {
            int rows = roleMapper.update(role);
            if (rows == 0) {
                throw new ServiceException("角色可能已被删除, 请刷新后重试!");
            }
        } catch (Exception ex) {
            LogUtil.error("编辑角色失败: ", ex);
            throw new ServiceException("角色名称重复, 请修改后重试!");
        }

        roleMapper.deleteRoleMenuRelation(role);
        if (!CollectionUtils.isEmpty(role.getMenuIds())) {
            int roleMenuRelationRows = roleMapper.insertRoleMenuRelation(role);
            if (roleMenuRelationRows != role.getMenuIds().size()) {
                throw new ServiceException("角色菜单关系数据写入失败, 请稍后重试!");
            }
        }
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void updateStatus(Role role) {
        int rows = roleMapper.updateStatus(role);
        if (rows == 0) {
            throw new ServiceException("角色可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public void delete(Role role) {
        int rows = roleMapper.delete(role);
        if (rows == 0) {
            throw new ServiceException("角色可能已被删除, 请刷新后重试!");
        }

        roleMapper.deleteRoleMenuRelation(role);
        roleMapper.deleteRoleUserRelation(role);
    }

    @Override
    public PageResult<Role> list(Page page, Role condition) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getSortStr());
        PageInfo<Role> pageResult = new PageInfo<>(roleMapper.list(condition));
        return PageResult.success(pageResult.getTotal(), pageResult.getList());
    }

    @Override
    public List<Role> enabledList() {
        Role condition = new Role();
        condition.setStatus(BasicDict.DataStatus.ENABLE);

        return roleMapper.list(condition);
    }
}