package pub.ikkyu.platform.service;

import org.springframework.web.multipart.MultipartFile;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.User;

/**
 * label: 用户管理 - 服务
 *
 * @author iktools
 */
public interface IUserService {

    /**
     * 新增用户
     *
     * @param user 用户数据
     */
    void insert(User user);

    /**
     * 按ID查询用户信息
     *
     * @param condition 查询条件
     * @return 符合条件的用户
     */
    User query(User condition);

    /**
     * 修改用户
     *
     * @param user 用户数据
     */
    void update(User user);

    /**
     * 修改用户状态
     *
     * @param user 用户数据
     */
    void updateStatus(User user);

    /**
     * 分页查询用户
     *
     * @param page      分页排序条件
     * @param condition 查询条件
     * @return 用户分页结果集
     */
    PageResult<User> list(Page page, User condition);

    /**
     * 按用户名查询用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    User queryUserByUsername(String username);

    /**
     * 查询用户权限信息
     *
     * @return 用户信息
     */
    User queryAuthorityInfo(User condition);

    /**
     * 按用户名更新用户信息
     *
     * @param user 用户信息
     */
    void updateUserByUsername(User user);

    /**
     * 用户头像上传
     *
     * @param file 图片文件
     * @return 文件路径
     */
    String uploadAvatar(MultipartFile file);
}