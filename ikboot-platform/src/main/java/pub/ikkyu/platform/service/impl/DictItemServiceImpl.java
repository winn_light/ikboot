package pub.ikkyu.platform.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import pub.ikkyu.core.cache.CacheHelper;
import pub.ikkyu.core.web.aspect.annotation.OperateInfoSet;
import pub.ikkyu.core.web.aspect.constants.OperatorType;
import pub.ikkyu.core.web.constants.BasicDict;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.core.web.exception.ServiceException;
import pub.ikkyu.platform.entity.DictItem;
import pub.ikkyu.platform.mapper.IDictItemMapper;
import pub.ikkyu.platform.service.IDictItemService;

import static pub.ikkyu.platform.constants.CacheAttribute.DATA_DICT_CACHE_KEY;

/**
 * label: 字典项管理 - 服务实现
 *
 * @author iktools
 */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class DictItemServiceImpl implements IDictItemService {

    @Autowired
    private IDictItemMapper dictItemMapper;

    @Autowired
    private CacheHelper cacheHelper;

    @Override
    @OperateInfoSet(OperatorType.CREATOR)
    public void insert(DictItem dictItem) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dictItem.getDictCode());

        validateMultipleValue(dictItem);

        dictItem.setStatus(BasicDict.DataStatus.ENABLE);
        int rows = dictItemMapper.insert(dictItem);
        if (rows == 0) {
            throw new ServiceException("字典项写入失败, 请稍后重试!");
        }
    }

    @Override
    @OperateInfoSet(OperatorType.MODIFIER)
    public void update(DictItem dictItem) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dictItem.getDictCode());

        validateMultipleValue(dictItem);

        int rows = dictItemMapper.update(dictItem);
        if (rows == 0) {
            throw new ServiceException("字典项可能已被删除, 请刷新后重试!");
        }
    }

    /**
     * 校验重复字典值
     *
     * @param condition 校验条件
     */
    private synchronized void validateMultipleValue(DictItem condition) {
        if (!StringUtils.hasText(condition.getDictCode())) {
            throw new ServiceException("所属字典编码不允许为空!");
        }

        if (!StringUtils.hasText(condition.getValue())) {
            throw new ServiceException("字典项值不允许为空!");
        }

        int multipleValueCount = dictItemMapper.countMultipleValue(condition);
        if (multipleValueCount > 0) {
            throw new ServiceException("同一字典的字典项值不允许重复, 请修改后重试!");
        }
    }

    @OperateInfoSet(OperatorType.MODIFIER)
    @Override
    public void updateStatus(DictItem dictItem) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dictItem.getDictCode());

        int rows = dictItemMapper.updateStatus(dictItem);
        if (rows == 0) {
            throw new ServiceException("字典项可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public void delete(DictItem dictItem) {
        cacheHelper.remove(DATA_DICT_CACHE_KEY, dictItem.getDictCode());

        int rows = dictItemMapper.delete(dictItem);
        if (rows == 0) {
            throw new ServiceException("字典项可能已被删除, 请刷新后重试!");
        }
    }

    @Override
    public PageResult<DictItem> list(Page page, DictItem condition) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getSortStr());
        PageInfo<DictItem> pageResult = new PageInfo<>(dictItemMapper.list(condition));
        return PageResult.success(pageResult.getTotal(), pageResult.getList());
    }

}