package pub.ikkyu.platform.entity;

import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.aspect.annotation.SortField;
import pub.ikkyu.core.web.entity.Tree;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;
import pub.ikkyu.platform.constants.PlatformDict;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.DataStatus;
import static pub.ikkyu.core.web.constants.BasicDict.Whether;
import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 菜单
 *
 * @author iktools
 */
public class Menu implements Tree, Serializable {

    private static final long serialVersionUID = -4247842492083257851L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, UpdateStatus.class, Delete.class, Query.class}, message = "{platform.menu.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 唯一编码
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.menu.code.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.menu.code.length}")
    @SortField("uk_code")
    private String code;

    /**
     * label: 名称
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.menu.name.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.menu.name.length}")
    private String name;

    /**
     * label: 菜单类型
     * notes: category-目录, menu-菜单, function-功能
     */
    @ValidDictValue(groups = {Insert.class}, value = PlatformDict.MenuType.class, required = true)
    private String type;

    /**
     * label: 菜单同级排序号ID
     */
    @NotNull(groups = {Insert.class, Update.class}, message = "{platform.menu.sortNo.not-null}")
    @Min(groups = {Insert.class, Update.class}, value = 1, message = "{platform.menu.sortNo.min}")
    private Integer sortNo;

    /**
     * label: 状态: 1-启用,0-停用
     */
    @ValidDictValue(groups = {UpdateStatus.class}, value = DataStatus.class, required = true)
    private String status;

    /**
     * label: 是否超管菜单: 1-是, 0-否
     */
    @ValidDictValue(groups = {Insert.class, Update.class}, value = Whether.class, required = true)
    private String admin;

    /**
     * label: 父级菜单ID
     */
    @Length(groups = {Insert.class}, message = "{platform.menu.parentId.length}")
    private String parentId;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.menu.comment.length}")
    private String comment;

    /**
     * 关联API集合
     */
    private Set<String> apis;

    /**
     * label: 子对象集合
     */
    private List<Menu> children;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<String> getApis() {
        return apis;
    }

    public void setApis(Set<String> apis) {
        this.apis = apis;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Menu.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("code='" + code + "'")
                .add("name='" + name + "'")
                .add("type='" + type + "'")
                .add("sortNo=" + sortNo)
                .add("status='" + status + "'")
                .add("admin='" + admin + "'")
                .add("parentId='" + parentId + "'")
                .add("comment='" + comment + "'")
                .add("apis=" + apis)
                .add("children=" + children)
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}