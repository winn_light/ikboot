package pub.ikkyu.platform.entity;

import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.aspect.annotation.SortField;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.DataStatus;
import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 字典
 *
 * @author iktools
 */
public class Dict implements Serializable {

    private static final long serialVersionUID = -5595840741139537032L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, UpdateStatus.class, Delete.class}, message = "{platform.dict.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 唯一编码
     */
    @NotBlank(groups = {Insert.class, Update.class, UpdateStatus.class, Delete.class}, message = "{platform.dict.code.not-blank}")
    @Length(groups = {Insert.class}, max = 60, message = "{platform.dict.code.length}")
    @SortField("uk_code")
    private String code;

    /**
     * label: 名称
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.dict.name.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.dict.name.length}")
    private String name;

    /**
     * label: 状态: 1-启用,0-停用
     */
    @ValidDictValue(groups = {UpdateStatus.class}, value = DataStatus.class, required = true)
    private String status;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.dict.comment.length}")
    private String comment;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Dict.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("code='" + code + "'")
                .add("name='" + name + "'")
                .add("status='" + status + "'")
                .add("comment='" + comment + "'")
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}