package pub.ikkyu.platform.entity;

import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.DataStatus;
import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 字典项
 *
 * @author iktools
 */
public class DictItem implements Serializable {

    private static final long serialVersionUID = -3581462189001851252L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, UpdateStatus.class, Delete.class}, message = "{platform.dict-item.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 显示文本
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.dict-item.label.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.dict-item.label.length}")
    private String label;

    /**
     * label: 字典值
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.dict-item.value.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.dict-item.value.length}")
    private String value;

    /**
     * label: 排序号
     */
    @NotNull(groups = {Insert.class, Update.class}, message = "{platform.dict-item.sortNo.not-null}")
    @Min(groups = {Insert.class, Update.class}, value = 1, message = "{platform.dict-item.sortNo.min}")
    private Integer sortNo;

    /**
     * label: 状态: 1-启用,0-停用
     */
    @ValidDictValue(groups = {UpdateStatus.class}, value = DataStatus.class, required = true)
    private String status;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.dict-item.comment.length}")
    private String comment;

    /**
     * label: 所属字典ID
     */
    @NotBlank(groups = {Insert.class, ListQuery.class}, message = "{platform.dict-item.dictId.not-blank}")
    @Length(groups = {Insert.class}, max = 32, message = "{platform.dict-item.dictId.length}")
    private String dictId;

    /**
     * label: 所属字典编码
     */
    @NotBlank(groups = {Insert.class, Update.class, UpdateStatus.class, Delete.class}, message = "{platform.dict-item.dictCode.not-blank}")
    @Length(groups = {Insert.class}, max = 60, message = "{platform.dict-item.dictCode.length}")
    private String dictCode;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DictItem.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("label='" + label + "'")
                .add("value='" + value + "'")
                .add("sortNo=" + sortNo)
                .add("status='" + status + "'")
                .add("comment='" + comment + "'")
                .add("dictId='" + dictId + "'")
                .add("dictCode='" + dictCode + "'")
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}