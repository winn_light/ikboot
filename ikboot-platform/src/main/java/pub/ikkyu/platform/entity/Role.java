package pub.ikkyu.platform.entity;

import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.aspect.annotation.SortField;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.DataStatus;
import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 角色
 *
 * @author iktools
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 1037873835047420692L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, UpdateStatus.class, Delete.class, Query.class}, message = "{platform.role.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 角色名称
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.role.name.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.role.name.length}")
    @SortField("uk_name")
    private String name;

    /**
     * label: 状态: 1-启用,0-停用
     */
    @ValidDictValue(groups = {UpdateStatus.class}, value = DataStatus.class, required = true)
    private String status;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.role.comment.length}")
    private String comment;

    /**
     * label: 关联菜单ID集合
     */
    private Set<String> menuIds;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<String> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(Set<String> menuIds) {
        this.menuIds = menuIds;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Role.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("status='" + status + "'")
                .add("comment='" + comment + "'")
                .add("menuIds=" + menuIds)
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}