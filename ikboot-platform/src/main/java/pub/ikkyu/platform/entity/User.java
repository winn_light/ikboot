package pub.ikkyu.platform.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.aspect.annotation.SortField;
import pub.ikkyu.core.web.validate.annotation.ValidDictValue;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.constants.BasicDict.DataStatus;
import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 用户
 *
 * @author iktools
 */
public class User implements Serializable {

    private static final long serialVersionUID = 5231434011499991141L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, UpdateStatus.class, Delete.class, Query.class}, message = "{platform.user.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 用户名
     */
    @SortField("uk_username")
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.user.username.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.user.username.length}")
    private String username;

    /**
     * label: 密码
     */
    @NotBlank(groups = {Insert.class}, message = "{platform.user.password.not-blank}")
    @Length(groups = {Insert.class}, max = 100, message = "{platform.user.password.length}")
    private String password;

    /**
     * label: 姓名
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.user.fullName.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.user.fullName.length}")
    private String fullName;

    /**
     * label: 所属机构ID
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.user.orgId.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 32, message = "{platform.user.orgId.length}")
    private String orgId;

    /**
     * label: 机构名称
     */
    private String orgName;

    /**
     * label: 手机号
     */
    @NotNull(groups = {Insert.class, Update.class}, message = "{platform.user.phonenumber.not-null}")
    @Length(groups = {Insert.class, Update.class}, max = 15, message = "{platform.user.phonenumber.length}")
    @Pattern(groups = {Insert.class, Update.class}, regexp = "(?:(?:\\+|00)86)?1[3-9]\\d{9}", message = "{platform.user.phonenumber.pattern}")
    private String phonenumber;

    /**
     * label: 邮箱
     */
    @Length(groups = {Insert.class, Update.class}, max = 100, message = "{platform.user.email.length}")
    @Pattern(groups = {Insert.class, Update.class}, regexp = "\\w{1,80}@[a-zA-Z0-9]{2,10}(?:\\.[a-z]{2,4}){1,3}", message = "{platform.user.email.pattern}")
    private String email;

    /**
     * label: 头像路径
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.user.avatar.length}")
    private String avatar;

    /**
     * label: 状态: 1-启用,0-停用
     */
    @ValidDictValue(groups = {UpdateStatus.class}, value = DataStatus.class, required = true)
    private String status;

    /**
     * label: 是否超管用户, 1-是, 0-否
     */
    private String admin;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.user.comment.length}")
    private String comment;

    /**
     * label: 关联角色ID集合
     */
    private Set<String> roleIds;

    /**
     * label: 可访问菜单编码集合
     */
    @JsonIgnore
    private Set<String> menus;

    /**
     * label: 可访问功能编码集合
     */
    @JsonIgnore
    private Map<String, Boolean> functions;

    /**
     * label: 可访问API接口集合
     */
    @JsonIgnore
    private Set<String> apis;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Set<String> roleIds) {
        this.roleIds = roleIds;
    }

    public Set<String> getMenus() {
        return menus;
    }

    public void setMenus(Set<String> menus) {
        this.menus = menus;
    }

    public Map<String, Boolean> getFunctions() {
        return functions;
    }

    public void setFunctions(Map<String, Boolean> functions) {
        this.functions = functions;
    }

    public Set<String> getApis() {
        return apis;
    }

    public void setApis(Set<String> apis) {
        this.apis = apis;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("username='" + username + "'")
                .add("password='" + password + "'")
                .add("fullName='" + fullName + "'")
                .add("orgId='" + orgId + "'")
                .add("orgName='" + orgName + "'")
                .add("phonenumber='" + phonenumber + "'")
                .add("email='" + email + "'")
                .add("avatar='" + avatar + "'")
                .add("status='" + status + "'")
                .add("admin='" + admin + "'")
                .add("comment='" + comment + "'")
                .add("roleIds=" + roleIds)
                .add("menus=" + menus)
                .add("functions=" + functions)
                .add("apis=" + apis)
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}