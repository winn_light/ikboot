package pub.ikkyu.platform.entity;

import org.hibernate.validator.constraints.Length;
import pub.ikkyu.core.mybatis.annotation.AutoSnowId;
import pub.ikkyu.core.web.entity.Tree;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 机构
 *
 * @author iktools
 */
public class Org implements Tree, Serializable {

    private static final long serialVersionUID = -4825489052630881265L;

    /**
     * label: ID
     */
    @NotBlank(groups = {Update.class, Delete.class}, message = "{platform.org.id.not-blank}")
    @AutoSnowId
    private String id;

    /**
     * label: 名称
     */
    @NotBlank(groups = {Insert.class, Update.class}, message = "{platform.org.name.not-blank}")
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.org.name.length}")
    private String name;

    /**
     * label: 排序号
     */
    @NotNull(groups = {Insert.class, Update.class}, message = "{platform.org.sortNo.not-null}")
    @Min(groups = {Insert.class, Update.class}, value = 1, message = "{platform.org.sortNo.min}")
    private Integer sortNo;

    /**
     * label: 负责人
     */
    @Length(groups = {Insert.class, Update.class}, max = 60, message = "{platform.org.leader.length}")
    private String leader;

    /**
     * label: 联系电话
     */
    @Length(groups = {Insert.class, Update.class}, max = 15, message = "{platform.org.phonenumber.length}")
    @Pattern(groups = {Insert.class, Update.class}, regexp = "((?:(?:\\d{3}-)?\\d{8}|^(?:\\d{4}-)?\\d{7,8})(?:-\\d+)?|(?:(?:\\+|00)86)?1[3-9]\\d{9})", message = "{platform.org.phonenumber.pattern}")
    private String phonenumber;

    /**
     * label: 联系邮箱
     */
    @Length(groups = {Insert.class, Update.class}, max = 100, message = "{platform.org.email.length}")
    @Pattern(groups = {Insert.class, Update.class}, regexp = "\\w{1,80}@[a-zA-Z0-9]{2,10}(?:\\.[a-z]{2,4}){1,3}", message = "{platform.org.email.pattern}")
    private String email;

    /**
     * label: 备注
     */
    @Length(groups = {Insert.class, Update.class}, max = 256, message = "{platform.org.comment.length}")
    private String comment;

    /**
     * label: 父级机构ID
     */
    @Length(groups = {Insert.class}, max = 32, message = "{platform.org.parentId.length}")
    private String parentId;

    /**
     * label: 子机构集合
     */
    private List<Org> children;

    /**
     * label: 创建人
     */
    private String createBy;

    /**
     * label: 创建时间
     */
    private Date createTime;

    /**
     * label: 修改人
     */
    private String updateBy;

    /**
     * label: 修改人
     */
    private Date updateTime;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<Org> getChildren() {
        return children;
    }

    public void setChildren(List<Org> children) {
        this.children = children;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Org.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("sortNo=" + sortNo)
                .add("leader='" + leader + "'")
                .add("phonenumber='" + phonenumber + "'")
                .add("email='" + email + "'")
                .add("comment='" + comment + "'")
                .add("parentId='" + parentId + "'")
                .add("children=" + children)
                .add("createBy='" + createBy + "'")
                .add("createTime=" + createTime)
                .add("updateBy='" + updateBy + "'")
                .add("updateTime=" + updateTime)
                .toString();
    }
}