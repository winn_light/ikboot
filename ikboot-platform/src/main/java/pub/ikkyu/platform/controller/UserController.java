package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.User;
import pub.ikkyu.platform.service.IUserService;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 用户管理
 *
 * @author iktools
 */
@RestController
@RequestMapping("/platform/user")
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * label: 新增用户
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) User user) {
        userService.insert(user);

        return AjaxResult.success();
    }

    /**
     * label: 查询用户
     */
    @GetMapping("/query")
    public AjaxResult<User> query(@Validated(Query.class) User user) {
        return AjaxResult.success(userService.query(user));
    }

    /**
     * label: 修改用户
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) User user) {
        userService.update(user);

        return AjaxResult.success();
    }

    /**
     * label: 修改用户状态
     */
    @PostMapping("/status/update")
    public AjaxResult<Void> updateStatus(@Validated(UpdateStatus.class) User user) {
        userService.updateStatus(user);

        return AjaxResult.success();
    }

    /**
     * label: 分页查询用户
     */
    @GetMapping("/list")
    public PageResult<User> list(@Validated Page page, User condition) {
        return userService.list(page, condition);
    }

    /**
     * label: 用户头像上传
     */
    @PostMapping("/avatar/upload")
    public AjaxResult<String> uploadAvatar(MultipartFile file) {
        return AjaxResult.success(userService.uploadAvatar(file));
    }

}