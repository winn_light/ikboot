package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.platform.entity.Menu;
import pub.ikkyu.platform.service.IMenuService;

import java.util.List;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 菜单管理
 *
 * @author iktools
 */
@RestController
@RequestMapping("/platform/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    /**
     * label: 新增菜单
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) Menu menu) {
        menuService.insert(menu);

        return AjaxResult.success();
    }

    /**
     * label: 查询菜单
     */
    @GetMapping("/query")
    public AjaxResult<Menu> query(@Validated(Query.class) Menu menu) {
        return AjaxResult.success(menuService.query(menu));
    }

    /**
     * label: 修改菜单
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) Menu menu) {
        menuService.update(menu);

        return AjaxResult.success();
    }

    /**
     * label: 修改菜单状态
     */
    @PostMapping("/status/update")
    public AjaxResult<Void> updateStatus(@Validated(UpdateStatus.class) Menu menu) {
        menuService.updateStatus(menu);

        return AjaxResult.success();
    }

    /**
     * label: 删除菜单
     */
    @PostMapping("/delete")
    public AjaxResult<Void> delete(@Validated(Delete.class) Menu menu) {
        menuService.delete(menu);

        return AjaxResult.success();
    }

    /**
     * label: 条件查询菜单树
     */
    @GetMapping("/tree/list")
    public AjaxResult<List<Menu>> listTree(Page page, Menu condition) {
        return AjaxResult.success(menuService.listTree(page, condition));
    }

}