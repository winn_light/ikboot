package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.Dict;
import pub.ikkyu.platform.entity.DictItem;
import pub.ikkyu.platform.service.IDictService;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 字典管理
 *
 * @author iktools
 */
@Validated
@RestController
@RequestMapping("/platform/dict")
public class DictController {

    @Autowired
    private IDictService dictService;

    /**
     * label: 新增字典
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) Dict dict) {
        dictService.insert(dict);

        return AjaxResult.success();
    }

    /**
     * label: 修改字典
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) Dict dict) {
        dictService.update(dict);

        return AjaxResult.success();
    }

    /**
     * label: 修改字典状态
     */
    @PostMapping("/status/update")
    public AjaxResult<Void> updateStatus(@Validated(UpdateStatus.class) Dict dict) {
        dictService.updateStatus(dict);

        return AjaxResult.success();
    }

    /**
     * label: 删除字典
     */
    @PostMapping("/delete")
    public AjaxResult<Void> delete(@Validated(Delete.class) Dict dict) {
        dictService.delete(dict);

        return AjaxResult.success();
    }

    /**
     * label: 分页查询字典
     */
    @GetMapping("/list")
    public PageResult<Dict> list(@Validated Page page, Dict condition) {
        return dictService.list(page, condition);
    }

    /**
     * label: 数据字典列表查询
     */
    @GetMapping("/data-dict/list")
    public AjaxResult<Map<String, List<DictItem>>> listDataDictByCodes(@Size(min = 1, message = "字典编码不允许为空!") String[] dictCodes) {
        return AjaxResult.success(dictService.listDataDictByCodes(dictCodes));
    }

}