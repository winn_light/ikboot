package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.platform.entity.Org;
import pub.ikkyu.platform.service.IOrgService;

import java.util.List;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 机构管理
 *
 * @author iktools
 */
@RestController
@RequestMapping("/platform/org")
public class OrgController {

    @Autowired
    private IOrgService orgService;

    /**
     * label: 新增机构
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) Org org) {
        orgService.insert(org);

        return AjaxResult.success();
    }

    /**
     * label: 修改机构
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) Org org) {
        orgService.update(org);

        return AjaxResult.success();
    }

    /**
     * label: 删除机构
     */
    @PostMapping("/delete")
    public AjaxResult<Void> delete(@Validated(Delete.class) Org org) {
        orgService.delete(org);

        return AjaxResult.success();
    }

    /**
     * label: 条件查询机构树
     */
    @GetMapping("/tree/list")
    public AjaxResult<List<Org>> listTree(Page page, Org condition) {
        return AjaxResult.success(orgService.listTree(page, condition));
    }

}