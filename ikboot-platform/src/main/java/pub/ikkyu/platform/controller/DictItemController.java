package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.DictItem;
import pub.ikkyu.platform.service.IDictItemService;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 字典项管理
 *
 * @author iktools
 */
@RestController
@RequestMapping("/platform/dict-item")
public class DictItemController {

    @Autowired
    private IDictItemService dictItemService;

    /**
     * label: 新增字典项
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) DictItem dictItem) {
        dictItemService.insert(dictItem);

        return AjaxResult.success();
    }

    /**
     * label: 修改字典项
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) DictItem dictItem) {
        dictItemService.update(dictItem);

        return AjaxResult.success();
    }

    /**
     * label: 修改字典项状态
     */
    @PostMapping("/status/update")
    public AjaxResult<Void> updateStatus(@Validated(UpdateStatus.class) DictItem dictItem) {
        dictItemService.updateStatus(dictItem);

        return AjaxResult.success();
    }

    /**
     * label: 删除字典项
     */
    @PostMapping("/delete")
    public AjaxResult<Void> delete(@Validated(Delete.class) DictItem dictItem) {
        dictItemService.delete(dictItem);

        return AjaxResult.success();
    }

    /**
     * label: 分页查询字典项
     */
    @GetMapping("/list")
    public PageResult<DictItem> list(@Validated Page page, @Validated(ListQuery.class) DictItem condition) {
        return dictItemService.list(page, condition);
    }

}