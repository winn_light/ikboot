package pub.ikkyu.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pub.ikkyu.core.web.entity.AjaxResult;
import pub.ikkyu.core.web.entity.Page;
import pub.ikkyu.core.web.entity.PageResult;
import pub.ikkyu.platform.entity.Role;
import pub.ikkyu.platform.service.IRoleService;

import java.util.List;

import static pub.ikkyu.core.web.validate.Rule.*;

/**
 * label: 角色管理
 *
 * @author iktools
 */
@RestController
@RequestMapping("/platform/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    /**
     * label: 新增角色
     */
    @PostMapping("/insert")
    public AjaxResult<Void> insert(@Validated(Insert.class) Role role) {
        roleService.insert(role);

        return AjaxResult.success();
    }

    /**
     * label: 查询角色
     */
    @GetMapping("/query")
    public AjaxResult<Role> query(@Validated(Query.class) Role role) {
        return AjaxResult.success(roleService.query(role));
    }

    /**
     * label: 修改角色
     */
    @PostMapping("/update")
    public AjaxResult<Void> update(@Validated(Update.class) Role role) {
        roleService.update(role);

        return AjaxResult.success();
    }

    /**
     * label: 修改角色状态
     */
    @PostMapping("/status/update")
    public AjaxResult<Void> updateStatus(@Validated(UpdateStatus.class) Role role) {
        roleService.updateStatus(role);

        return AjaxResult.success();
    }

    /**
     * label: 删除角色
     */
    @PostMapping("/delete")
    public AjaxResult<Void> delete(@Validated(Delete.class) Role role) {
        roleService.delete(role);

        return AjaxResult.success();
    }

    /**
     * label: 分页查询角色
     */
    @GetMapping("/list")
    public PageResult<Role> list(@Validated Page page, Role condition) {
        return roleService.list(page, condition);
    }

    /**
     * label: 查询启用的所有角色
     */
    @GetMapping("/enabled/list")
    public AjaxResult<List<Role>> enabledList() {
        return AjaxResult.success(roleService.enabledList());
    }

}