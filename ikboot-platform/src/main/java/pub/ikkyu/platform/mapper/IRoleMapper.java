package pub.ikkyu.platform.mapper;

import pub.ikkyu.platform.entity.Role;

import java.util.List;
import java.util.Set;

/**
 * label: 角色管理 - 持久
 *
 * @author iktools
 */
public interface IRoleMapper {

    /**
     * 统计重复角色名称数
     *
     * @param condition 统计条件
     * @return 受影响行数
     */
    int countMultipleName(Role condition);

    /**
     * 新增角色
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int insert(Role role);

    /**
     * 按ID查询角色信息
     *
     * @param condition 查询条件
     * @return 角色数据
     */
    Role query(Role condition);

    /**
     * 列表查询角色关联菜单ID
     *
     * @param condition 查询条件
     * @return 菜单ID集合
     */
    Set<String> listRoleMenu(Role condition);

    /**
     * 删除角色菜单关系数据
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int deleteRoleMenuRelation(Role role);

    /**
     * 写入角色菜单关系数据
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int insertRoleMenuRelation(Role role);

    /**
     * 修改角色
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int update(Role role);

    /**
     * 修改角色状态
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int updateStatus(Role role);

    /**
     * 删除角色
     *
     * @param role 角色数据
     * @return 受影响行数
     */
    int delete(Role role);

    /**
     * 删除角色用户关系数据
     *
     * @param role 角色条件
     */
    void deleteRoleUserRelation(Role role);

    /**
     * 列表查询角色
     *
     * @param condition 查询条件
     * @return 角色列表
     */
    List<Role> list(Role condition);
}