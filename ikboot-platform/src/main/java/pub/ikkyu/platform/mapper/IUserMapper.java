package pub.ikkyu.platform.mapper;

import org.apache.ibatis.annotations.Param;
import pub.ikkyu.platform.entity.Menu;
import pub.ikkyu.platform.entity.User;

import java.util.List;
import java.util.Set;

/**
 * label: 用户管理 - 持久
 *
 * @author iktools
 */
public interface IUserMapper {

    /**
     * 统计重复用户名数
     *
     * @param condition 统计条件
     * @return 重复用户名数
     */
    int countMultipleUsername(User condition);

    /**
     * 新增用户
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int insert(User user);

    /**
     * 删除用户角色关系数据
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int deleteUserRoleRelation(User user);

    /**
     * 写入用户角色关系数据
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int insertUserRoleRelation(User user);

    /**
     * 按id查询用户基本信息
     *
     * @param condition 查询条件
     * @return 用户数据
     */
    User query(User condition);

    /**
     * 按id查询用户关联的所有角色
     *
     * @param condition 查询条件
     * @return 关联角色ID集合
     */
    Set<String> queryRoleIds(User condition);

    /**
     * 修改用户
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int update(User user);

    /**
     * 修改用户状态
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int updateStatus(User user);

    /**
     * 删除用户
     *
     * @param user 用户数据
     * @return 受影响行数
     */
    int delete(User user);

    /**
     * 列表查询用户
     *
     * @param condition 查询条件
     * @return 用户列表
     */
    List<User> list(User condition);

    /**
     * 按登录名查询用户
     *
     * @param condition 查询条件
     * @return 用户信息
     */
    User queryByUsername(User condition);

    /**
     * 按用户名查询用户权限列表
     *
     * @param condition 查询条件
     * @return 权限列表
     */
    List<Menu> queryAuthorityByUsername(User condition);

    /**
     * 按功能ID集合查询可访问API接口
     *
     * @param functionIds 功能ID集合
     * @return 可访问API接口集合
     */
    Set<String> queryApiByFunctionIds(@Param("list") Set<String> functionIds);

    /**
     * 按用户名更新用户信息
     *
     * @param user 用户信息
     * @return 受影响行数
     */
    int updateByUsername(User user);
}