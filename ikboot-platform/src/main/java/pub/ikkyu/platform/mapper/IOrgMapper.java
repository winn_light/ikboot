package pub.ikkyu.platform.mapper;

import pub.ikkyu.platform.entity.Org;

import java.util.List;

/**
 * label: 机构管理 - 持久
 *
 * @author iktools
 */
public interface IOrgMapper {

    /**
     * 统计同级次重复机构名称数
     *
     * @param condition 统计条件
     * @return 同级次重复机构名称数
     */
    int countSameLevelMultipleName(Org condition);

    /**
     * 新增机构
     *
     * @param org 机构数据
     * @return 受影响行数
     */
    int insert(Org org);

    /**
     * 修改机构
     *
     * @param org 机构数据
     * @return 受影响行数
     */
    int update(Org org);

    /**
     * 统计子机构数
     *
     * @param condition 统计条件
     * @return 子机构数
     */
    int countSonOrg(Org condition);

    /**
     * 统计已关联用户数
     *
     * @param org 所属机构
     * @return 已关联用户数
     */
    int countRelatedUser(Org org);

    /**
     * 删除机构
     *
     * @param org 机构数据
     * @return 受影响行数
     */
    int delete(Org org);

    /**
     * 列表查询机构
     *
     * @param condition 查询条件
     * @return 机构列表
     */
    List<Org> list(Org condition);
}