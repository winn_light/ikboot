package pub.ikkyu.platform.mapper;

import org.apache.ibatis.annotations.Param;
import pub.ikkyu.platform.entity.Menu;

import java.util.List;
import java.util.Set;

/**
 * label: 菜单管理 - 持久
 *
 * @author iktools
 */
public interface IMenuMapper {

    /**
     * 重复菜单编码数统计
     *
     * @param condition 统计条件
     * @return 重复菜单编码数
     */
    int countMultipleCode(Menu condition);

    /**
     * 新增菜单
     *
     * @param menu 菜单数据
     * @return 受影响行数
     */
    int insert(Menu menu);

    /**
     * 查询菜单数据
     *
     * @param condition 查询条件
     * @return 菜单数据
     */
    Menu query(Menu condition);

    /**
     * 按ID查询菜单关联API接口
     *
     * @param condition 查询条件
     * @return 菜单关联API数据
     */
    Set<String> listMenuApi(Menu condition);

    /**
     * 修改菜单
     *
     * @param menu 菜单数据
     * @return 受影响行数
     */
    int update(Menu menu);

    /**
     * 删除菜单关联API关系数据
     *
     * @param condition 删除条件
     * @return 受影响行数
     */
    int deleteMenuApiRelation(Menu condition);

    /**
     * 写入菜单API关系数据
     *
     * @param menu 菜单数据
     * @return 受影响行数
     */
    int insertMenuApiRelation(Menu menu);

    /**
     * 查询子菜单ID集合
     *
     * @param parentIds 父菜单ID集合
     * @return 子菜单ID集合
     */
    List<String> listChildrenIds(@Param("parentIds") List<String> parentIds);

    /**
     * 修改菜单状态
     *
     * @param ids  待变更状态菜单ID集合
     * @param menu 变更后的状态信息
     * @return 受影响行数
     */
    int updateStatus(@Param("ids") Set<String> ids, @Param("menu") Menu menu);

    /**
     * 统计子菜单数
     *
     * @param condition 统计条件
     * @return 子菜单数
     */
    int countSonMenu(Menu condition);

    /**
     * 删除菜单
     *
     * @param menu 菜单数据
     * @return 受影响行数
     */
    int delete(Menu menu);

    /**
     * 删除角色关联菜单关系数据
     *
     * @param condition 删除条件
     */
    void deleteRoleMenuRelation(Menu condition);

    /**
     * 列表查询菜单
     *
     * @param condition 查询条件
     * @return 菜单列表
     */
    List<Menu> list(Menu condition);
}