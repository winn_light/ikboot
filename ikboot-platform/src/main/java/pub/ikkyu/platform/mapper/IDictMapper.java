package pub.ikkyu.platform.mapper;

import pub.ikkyu.platform.entity.Dict;

import java.util.List;

/**
 * label: 字典管理 - 持久
 *
 * @author iktools
 */
public interface IDictMapper {

    /**
     * 重复编码计数
     *
     * @param dict 字典数据
     * @return 重复编码数
     */
    int countMultipleCode(Dict dict);

    /**
     * 新增字典
     *
     * @param dict 字典数据
     * @return 受影响行数
     */
    int insert(Dict dict);

    /**
     * 修改字典
     *
     * @param dict 字典数据
     * @return 受影响行数
     */
    int update(Dict dict);

    /**
     * 修改字典状态
     *
     * @param dict 字典数据
     * @return 受影响行数
     */
    int updateStatus(Dict dict);

    /**
     * 删除字典
     *
     * @param dict 字典数据
     * @return 受影响行数
     */
    int delete(Dict dict);

    /**
     * 列表查询字典
     *
     * @param condition 查询条件
     * @return 字典列表
     */
    List<Dict> list(Dict condition);
}