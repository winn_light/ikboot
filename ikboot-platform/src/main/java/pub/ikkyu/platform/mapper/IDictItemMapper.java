package pub.ikkyu.platform.mapper;

import pub.ikkyu.platform.entity.DictItem;

import java.util.List;

/**
 * label: 字典项管理 - 持久
 *
 * @author iktools
 */
public interface IDictItemMapper {

    /**
     * 同一字典下的重复字典值数
     *
     * @param condition 统计条件
     * @return 重复值数
     */
    int countMultipleValue(DictItem condition);

    /**
     * 新增字典项
     *
     * @param dictItem 字典项数据
     * @return 受影响行数
     */
    int insert(DictItem dictItem);

    /**
     * 修改字典项
     *
     * @param dictItem 字典项数据
     * @return 受影响行数
     */
    int update(DictItem dictItem);

    /**
     * 修改字典项状态
     *
     * @param dictItem 字典项数据
     * @return 受影响行数
     */
    int updateStatus(DictItem dictItem);

    /**
     * 删除字典项
     * id不为空, 删除指定字典项
     * dictId不为空, 删除指定字典的所有字典项
     *
     * @param dictItem 字典项数据
     * @return 受影响行数
     */
    int delete(DictItem dictItem);

    /**
     * 列表查询字典项
     *
     * @param condition 查询条件
     * @return 字典项列表
     */
    List<DictItem> list(DictItem condition);

    /**
     * 按字典编码集合查询字典项集合
     *
     * @param dictCodeList 字典编码集合
     * @return 字典项集合
     */
    List<DictItem> listByDictCodes(List<String> dictCodeList);
}