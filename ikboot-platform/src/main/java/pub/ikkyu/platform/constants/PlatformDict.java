package pub.ikkyu.platform.constants;

/**
 * 平台管理字典
 */
public interface PlatformDict {

    /**
     * 菜单类型
     */
    interface MenuType {

        /**
         * 目录
         */
        String CATEGORY = "category";

        /**
         * 菜单
         */
        String MENU = "menu";

        /**
         * 功能
         */
        String FUNCTION = "function";

    }

}
