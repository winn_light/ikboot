package pub.ikkyu.platform.constants;

/**
 * 缓存KEY常量
 *
 * @author ikkyu
 */
public interface CacheAttribute {

    /**
     * 数据字典缓存
     */
    String DATA_DICT_CACHE_KEY = "PLATFORM:DATA-DICT#3600";

}
