-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: ikboot
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict`
(
    `pk_id`       varchar(32) NOT NULL COMMENT 'PK',
    `uk_code`     varchar(60)  DEFAULT '' COMMENT '唯一编码',
    `name`        varchar(60)  DEFAULT NULL COMMENT '名称',
    `status`      varchar(1)   DEFAULT NULL COMMENT '状态 1-启用,0-停用',
    `comment`     varchar(256) DEFAULT NULL COMMENT '备注',
    `create_by`   varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`pk_id`),
    UNIQUE KEY `uk_code` (`uk_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据字典 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK
TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict`
VALUES ('03fcd583fb86d000', 'data-status', '数据状态', '1', NULL, 'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('04004c26b8d6f000', 'whether', '是否项', '1', NULL, 'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('040c19469ada4000', 'menu-type', '菜单类型', '1', NULL, 'user-mock', '2021-07-01 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_dict_item`
--

DROP TABLE IF EXISTS `sys_dict_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_item`
(
    `pk_id`        varchar(32) NOT NULL COMMENT 'PK',
    `label`        varchar(60)  DEFAULT NULL COMMENT '显示文本',
    `value`        varchar(60)  DEFAULT NULL COMMENT '字典值',
    `sort_no`      int(11) DEFAULT NULL COMMENT '排序号',
    `status`       varchar(1)   DEFAULT NULL COMMENT '状态 1-启用,0-停用',
    `comment`      varchar(256) DEFAULT NULL COMMENT '备注',
    `fk_dict_id`   varchar(32)  DEFAULT NULL COMMENT '所属字典PK',
    `fk_dict_code` varchar(60)  DEFAULT NULL COMMENT '所属字典编码',
    `create_by`    varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time`  datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`pk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据字典项 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_item`
--

LOCK
TABLES `sys_dict_item` WRITE;
/*!40000 ALTER TABLE `sys_dict_item` DISABLE KEYS */;
INSERT INTO `sys_dict_item`
VALUES ('03fced468286d000', '启用', '1', 1, '1', NULL, '03fcd583fb86d000', 'data-status', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('03fced4e3646d000', '停用', '0', 2, '1', NULL, '03fcd583fb86d000', 'data-status', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('04004c326bd6f000', '是', '1', 1, '1', NULL, '04004c26b8d6f000', 'whether', 'user-mock', '2021-07-01 00:00:00',
        NULL, NULL),
       ('04004c6d8bd6f000', '否', '0', 2, '1', NULL, '04004c26b8d6f000', 'whether', 'user-mock', '2021-07-01 00:00:00',
        NULL, NULL),
       ('040c197db2da4000', '目录', 'category', 1, '1', NULL, '040c19469ada4000', 'menu-type', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040c198428da4000', '菜单', 'menu', 1, '1', NULL, '040c19469ada4000', 'menu-type', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040c198ade1a4000', '功能', 'function', 1, '1', NULL, '040c19469ada4000', 'menu-type', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `sys_dict_item` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu`
(
    `pk_id`        varchar(32) NOT NULL COMMENT 'PK',
    `uk_code`      varchar(60)  DEFAULT NULL COMMENT '唯一编码',
    `name`         varchar(60)  DEFAULT NULL COMMENT '名称',
    `type`         varchar(10)  DEFAULT NULL COMMENT '菜单类型, catagory-目录, menu-菜单, function-功能',
    `sort_no`      int(11) DEFAULT NULL COMMENT '排序号',
    `status`       varchar(1)   DEFAULT NULL COMMENT '状态 1-启用,0-停用',
    `is_admin`     varchar(1)   DEFAULT NULL COMMENT '是否管理员菜单, 1-是, 0-否',
    `fk_parent_id` varchar(32)  DEFAULT NULL COMMENT '父级ID',
    `comment`      varchar(256) DEFAULT NULL COMMENT '备注',
    `create_by`    varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time`  datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`pk_id`),
    UNIQUE KEY `uk_code` (`uk_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK
TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu`
VALUES ('040c2c86ba1a4000', 'platform', '平台管理', 'category', 1, '1', '0', '0', NULL, 'user-mock', '2021-07-01 00:00:00',
        NULL, NULL),
       ('040ea604af06a000', 'dict', '字典管理', 'menu', 1, '1', '1', '040c2c86ba1a4000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040ea6d535c6a000', 'dict-item', '字典项管理', 'menu', 2, '1', '1', '040c2c86ba1a4000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040ea6e00b06a000', 'menu', '菜单管理', 'menu', 3, '1', '1', '040c2c86ba1a4000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040ea6ead146a000', 'org', '机构管理', 'menu', 4, '1', '0', '040c2c86ba1a4000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040ea7022286a000', 'role', '角色管理', 'menu', 5, '1', '0', '040c2c86ba1a4000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('040ea70f2586a000', 'user', '用户管理', 'menu', 6, '1', '0', '040c2c86ba1a4000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412dd258dc6d000', 'platform.dict.add', '新增字典', 'function', 1, '1', '1', '040ea604af06a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412dd49e406d000', 'platform.dict.edit', '编辑字典', 'function', 2, '1', '1', '040ea604af06a000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412ddc65906d000', 'platform.dict.status.edit', '启停字典', 'function', 3, '1', '1', '040ea604af06a000', '',
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412ddf008c6d000', 'platform.dict.delete', '删除字典', 'function', 4, '1', '1', '040ea604af06a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412de04ca06d000', 'platform.dict.list', '查询字典', 'function', 5, '1', '1', '040ea604af06a000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412de20d006d000', 'platform.dict-item.add', '新增字典项', 'function', 1, '1', '1', '040ea6d535c6a000', '',
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412de3be686d000', 'platform.dict-item.edit', '编辑字典项', 'function', 2, '1', '1', '040ea6d535c6a000', '',
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412de556e06d000', 'platform.dict-item.status.edit', '启停字典项', 'function', 3, '1', '1', '040ea6d535c6a000', '',
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412de6db9c6d000', 'platform.dict-item.delete', '删除字典项', 'function', 4, '1', '1', '040ea6d535c6a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412de88b386d000', 'platform.dict-item.list', '查询字典项', 'function', 5, '1', '1', '040ea6d535c6a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412deb3db46d000', 'platform.menu.add', '新增菜单', 'function', 1, '1', '1', '040ea6e00b06a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412df457486d000', 'platform.menu.edit', '编辑菜单', 'function', 2, '1', '1', '040ea6e00b06a000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('0412df6feb06d000', 'platform.menu.status.edit', '启停菜单', 'function', 3, '1', '1', '040ea6e00b06a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412df878606d000', 'platform.menu.delete', '删除菜单', 'function', 4, '1', '1', '040ea6e00b06a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('0412dfa57706d000', 'platform.menu.list', '查询菜单', 'function', 5, '1', '1', '040ea6e00b06a000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3cbe7146b000', 'platform.org.add', '新增机构', 'function', 1, '1', '0', '040ea6ead146a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3cd515c6b000', 'platform.org.edit', '编辑机构', 'function', 2, '1', '0', '040ea6ead146a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3cecda46b000', 'platform.org.delete', '删除机构', 'function', 3, '1', '0', '040ea6ead146a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d0bf906b000', 'platform.org.list', '查询机构', 'function', 4, '1', '0', '040ea6ead146a000', '', 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d2a8b46b000', 'platform.role.add', '新增角色', 'function', 1, '1', '0', '040ea7022286a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d483846b000', 'platform.role.edit', '编辑角色', 'function', 2, '1', '0', '040ea7022286a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d6246c6b000', 'platform.role.status.edit', '启停角色', 'function', 3, '1', '0', '040ea7022286a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d7a6206b000', 'platform.role.delete', '删除角色', 'function', 4, '1', '0', '040ea7022286a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3d8e6f06b000', 'platform.role.list', '查询角色', 'function', 5, '1', '0', '040ea7022286a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3da90086b000', 'platform.user.add', '新增用户', 'function', 1, '1', '0', '040ea70f2586a000', NULL, 'user-mock',
        '2021-07-01 00:00:00', NULL, NULL),
       ('041f3dc0bfc6b000', 'platform.user.edit', '编辑用户', 'function', 2, '1', '0', '040ea70f2586a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3ddaa606b000', 'platform.user.status.edit', '启停用户', 'function', 3, '1', '0', '040ea70f2586a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL),
       ('041f3dfe23c6b000', 'platform.user.list', '查询用户', 'function', 4, '1', '0', '040ea70f2586a000', NULL,
        'user-mock', '2021-07-01 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_menu_api_relation`
--

DROP TABLE IF EXISTS `sys_menu_api_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu_api_relation`
(
    `fk_menu_id` varchar(32)  NOT NULL COMMENT '菜单ID',
    `fk_api_uri` varchar(256) NOT NULL COMMENT 'API-URI',
    PRIMARY KEY (`fk_menu_id`, `fk_api_uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单API关系 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu_api_relation`
--

LOCK
TABLES `sys_menu_api_relation` WRITE;
/*!40000 ALTER TABLE `sys_menu_api_relation` DISABLE KEYS */;
INSERT INTO `sys_menu_api_relation`
VALUES ('0412dd258dc6d000', '/platform/dict/insert'),
       ('0412dd49e406d000', '/platform/dict/update'),
       ('0412ddc65906d000', '/platform/dict/status/update'),
       ('0412ddf008c6d000', '/platform/dict/delete'),
       ('0412de04ca06d000', '/platform/dict/list'),
       ('0412de20d006d000', '/platform/dict-item/insert'),
       ('0412de3be686d000', '/platform/dict-item/update'),
       ('0412de556e06d000', '/platform/dict-item/status/update'),
       ('0412de6db9c6d000', '/platform/dict-item/delete'),
       ('0412de88b386d000', '/platform/dict-item/list'),
       ('0412deb3db46d000', '/platform/menu/insert'),
       ('0412df457486d000', '/platform/menu/query'),
       ('0412df457486d000', '/platform/menu/update'),
       ('0412df6feb06d000', '/platform/menu/status/update'),
       ('0412df878606d000', '/platform/menu/delete'),
       ('0412dfa57706d000', '/platform/menu/tree/list'),
       ('0412e0593346d000', '/platform/test/son'),
       ('041f3cbe7146b000', '/platform/org/insert'),
       ('041f3cd515c6b000', '/platform/org/update'),
       ('041f3cecda46b000', '/platform/org/delete'),
       ('041f3d0bf906b000', '/platform/org/tree/list'),
       ('041f3d2a8b46b000', '/platform/menu/tree/list'),
       ('041f3d2a8b46b000', '/platform/role/insert'),
       ('041f3d483846b000', '/platform/menu/tree/list'),
       ('041f3d483846b000', '/platform/role/query'),
       ('041f3d483846b000', '/platform/role/update'),
       ('041f3d6246c6b000', '/platform/role/status/update'),
       ('041f3d7a6206b000', '/platform/role/delete'),
       ('041f3d8e6f06b000', '/platform/role/list'),
       ('041f3da90086b000', '/platform/role/enabled/list'),
       ('041f3da90086b000', '/platform/user/insert'),
       ('041f3dc0bfc6b000', '/platform/user/query'),
       ('041f3dc0bfc6b000', '/platform/role/enabled/list'),
       ('041f3dc0bfc6b000', '/platform/user/update'),
       ('041f3ddaa606b000', '/platform/user/status/update'),
       ('041f3dfe23c6b000', '/platform/user/list');
/*!40000 ALTER TABLE `sys_menu_api_relation` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_org`
--

DROP TABLE IF EXISTS `sys_org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_org`
(
    `pk_id`        varchar(32) NOT NULL COMMENT 'PK',
    `name`         varchar(60)  DEFAULT NULL COMMENT '名称',
    `sort_no`      int(11) DEFAULT NULL COMMENT '同级排序号',
    `leader`       varchar(60)  DEFAULT NULL COMMENT '负责人',
    `phonenumber`  varchar(15)  DEFAULT NULL COMMENT '联系电话',
    `email`        varchar(100) DEFAULT NULL COMMENT '联系邮箱',
    `comment`      varchar(256) DEFAULT NULL COMMENT '备注',
    `fk_parent_id` varchar(32)  DEFAULT NULL COMMENT '父级机构ID',
    `create_by`    varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time`  datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`pk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='组织机构 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_org`
--

LOCK
TABLES `sys_org` WRITE;
/*!40000 ALTER TABLE `sys_org` DISABLE KEYS */;
INSERT INTO `sys_org`
VALUES ('041f45f86b86b000', 'IK-WEB', 1, '', NULL, NULL, NULL, '0', 'user-mock', '2021-07-01 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `sys_org` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role`
(
    `pk_id`       varchar(32) NOT NULL COMMENT 'PK',
    `uk_name`     varchar(60)  DEFAULT NULL COMMENT '唯一名称',
    `status`      varchar(1)   DEFAULT NULL COMMENT '状态 1-启用,0-停用',
    `comment`     varchar(256) DEFAULT NULL COMMENT '备注',
    `create_by`   varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`pk_id`),
    UNIQUE KEY `uk_name` (`uk_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK
TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role`
VALUES ('041f43603452a000', '系统管理员', '1', '', 'user-mock', '2021-07-01 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_role_menu_relation`
--

DROP TABLE IF EXISTS `sys_role_menu_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu_relation`
(
    `fk_role_id` varchar(32) NOT NULL COMMENT '角色ID',
    `fk_menu_id` varchar(32) NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`fk_role_id`, `fk_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单关系 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu_relation`
--

LOCK
TABLES `sys_role_menu_relation` WRITE;
/*!40000 ALTER TABLE `sys_role_menu_relation` DISABLE KEYS */;
INSERT INTO `sys_role_menu_relation`
VALUES ('041f43603452a000', '040c2c86ba1a4000'),
       ('041f43603452a000', '040ea6ead146a000'),
       ('041f43603452a000', '040ea7022286a000'),
       ('041f43603452a000', '040ea70f2586a000'),
       ('041f43603452a000', '041f3cbe7146b000'),
       ('041f43603452a000', '041f3cd515c6b000'),
       ('041f43603452a000', '041f3cecda46b000'),
       ('041f43603452a000', '041f3d0bf906b000'),
       ('041f43603452a000', '041f3d2a8b46b000'),
       ('041f43603452a000', '041f3d483846b000'),
       ('041f43603452a000', '041f3d6246c6b000'),
       ('041f43603452a000', '041f3d7a6206b000'),
       ('041f43603452a000', '041f3d8e6f06b000'),
       ('041f43603452a000', '041f3da90086b000'),
       ('041f43603452a000', '041f3dc0bfc6b000'),
       ('041f43603452a000', '041f3ddaa606b000'),
       ('041f43603452a000', '041f3dfe23c6b000');
/*!40000 ALTER TABLE `sys_role_menu_relation` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user`
(
    `pk_id`          varchar(100) NOT NULL COMMENT 'PK',
    `uk_username`    varchar(60)  DEFAULT NULL COMMENT '用户名',
    `full_name`      varchar(60)  DEFAULT NULL COMMENT '姓名',
    `password`       varchar(100) DEFAULT NULL COMMENT '密码',
    `fk_org_id`      varchar(32)  DEFAULT NULL COMMENT '所属机构ID',
    `uk_phonenumber` varchar(15)  DEFAULT NULL COMMENT '手机号',
    `email`          varchar(100) DEFAULT NULL COMMENT '邮箱',
    `avatar`         varchar(256) DEFAULT NULL COMMENT '头像路径',
    `status`         varchar(1)   DEFAULT NULL COMMENT '状态 1-启用,0-停用',
    `comment`        varchar(256) DEFAULT NULL COMMENT '备注',
    `create_by`      varchar(60)  DEFAULT NULL COMMENT '创建人',
    `create_time`    datetime     DEFAULT NULL COMMENT '创建时间',
    `update_by`      varchar(60)  DEFAULT NULL COMMENT '更新人',
    `update_time`    datetime     DEFAULT NULL COMMENT '更新时间',
    `is_admin`       varchar(1)   DEFAULT NULL COMMENT '是否管理员',
    PRIMARY KEY (`pk_id`),
    UNIQUE KEY `uk_username` (`uk_username`),
    UNIQUE KEY `uk_phonenumber` (`uk_phonenumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK
TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user`
VALUES ('0421f6320c630000', 's-admin', '超管', '$2a$10$4xK4nI9P6q3H3mMUGFFByOz7D.9ESOfYP9S9SO1dz4CmkeBhNJdKG',
        '041f45f86b86b000', NULL, NULL, NULL, '1', NULL, 'user-mock', '2021-07-01 00:00:00', NULL, NULL, '1'),
       ('0421f63e7be30000', 'admin', '系统管理员', '$2a$10$sgPYzYRdK3gjkbeN3NvEQ.f5b6RLW6/qDPr5ZjOK16c2ih4vabSv2',
        '041f45f86b86b000', NULL, NULL, NULL, '1', NULL, 'user-mock', '2021-07-01 00:00:00', NULL, NULL, '0');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK
TABLES;

--
-- Table structure for table `sys_user_role_relation`
--

DROP TABLE IF EXISTS `sys_user_role_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role_relation`
(
    `fk_user_id` varchar(32) NOT NULL COMMENT '用户ID',
    `fk_role_id` varchar(32) NOT NULL COMMENT '角色ID',
    PRIMARY KEY (`fk_user_id`, `fk_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关系 ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role_relation`
--

LOCK
TABLES `sys_user_role_relation` WRITE;
/*!40000 ALTER TABLE `sys_user_role_relation` DISABLE KEYS */;
INSERT INTO `sys_user_role_relation`
VALUES ('0421f63e7be30000', '041f43603452a000');
/*!40000 ALTER TABLE `sys_user_role_relation` ENABLE KEYS */;
UNLOCK
TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-14 21:45:34
