IF EXISTS(SELECT * FROM SYSDATABASES WHERE name = 'ikboot')
DROP DATABASE ikboot
GO
CREATE DATABASE ikboot
GO
USE [ikboot]
GO
/****** Object:  Table [dbo].[sys_user_role_relation]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_user_role_relation](
	[fk_user_id] [varchar](32) NOT NULL,
	[fk_role_id] [varchar](32) NOT NULL,
 CONSTRAINT [PK_system_user_role_relation] PRIMARY KEY CLUSTERED 
(
	[fk_user_id] ASC,
	[fk_role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_role_relation', @level2type=N'COLUMN',@level2name=N'fk_user_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_role_relation', @level2type=N'COLUMN',@level2name=N'fk_role_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户角色关系' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user_role_relation'
GO
INSERT [dbo].[sys_user_role_relation] ([fk_user_id], [fk_role_id]) VALUES (N'0421f63e7be30000', N'041f43603452a000')
/****** Object:  Table [dbo].[sys_user]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_user](
	[pk_id] [varchar](32) NOT NULL,
	[uk_username] [varchar](60) NOT NULL,
	[full_name] [varchar](60) NULL,
	[password] [varchar](100) NULL,
	[fk_org_id] [varchar](32) NULL,
	[uk_phonenumber] [varchar](15) NOT NULL,
	[email] [varchar](100) NULL,
	[avatar] [varchar](256) NULL,
	[status] [varchar](1) NULL,
	[comment] [varchar](256) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
	[is_admin] [varchar](1) NULL,
 CONSTRAINT [PK_system_user] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_system_user_phonenumber] UNIQUE NONCLUSTERED 
(
	[uk_phonenumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_system_user_username] UNIQUE NONCLUSTERED 
(
	[uk_username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'uk_username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'full_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属机构ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'fk_org_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'uk_phonenumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'avatar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态 1-启用,0-停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'is_admin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user'
GO
INSERT [dbo].[sys_user] ([pk_id], [uk_username], [full_name], [password], [fk_org_id], [uk_phonenumber], [email], [avatar], [status], [comment], [create_by], [create_time], [update_by], [update_time], [is_admin]) VALUES (N'0421f6320c630000', N's-admin', N'超管', N'$2a$10$4xK4nI9P6q3H3mMUGFFByOz7D.9ESOfYP9S9SO1dz4CmkeBhNJdKG', N'041f45f86b86b000', N'911', NULL, NULL, N'1', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL, N'1')
INSERT [dbo].[sys_user] ([pk_id], [uk_username], [full_name], [password], [fk_org_id], [uk_phonenumber], [email], [avatar], [status], [comment], [create_by], [create_time], [update_by], [update_time], [is_admin]) VALUES (N'0421f63e7be30000', N'admin', N'系统管理员', N'$2a$10$sgPYzYRdK3gjkbeN3NvEQ.f5b6RLW6/qDPr5ZjOK16c2ih4vabSv2', N'041f45f86b86b000', N'912', NULL, NULL, N'1', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL, N'0')
/****** Object:  Table [dbo].[sys_role_menu_relation]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_role_menu_relation](
	[fk_role_id] [varchar](32) NOT NULL,
	[fk_menu_id] [varchar](32) NOT NULL,
 CONSTRAINT [PK_system_role_menu_relation] PRIMARY KEY CLUSTERED 
(
	[fk_role_id] ASC,
	[fk_menu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu_relation', @level2type=N'COLUMN',@level2name=N'fk_role_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu_relation', @level2type=N'COLUMN',@level2name=N'fk_menu_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色菜单关系' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu_relation'
GO
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'040c2c86ba1a4000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'040ea6ead146a000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'040ea7022286a000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'040ea70f2586a000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3cbe7146b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3cd515c6b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3cecda46b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d0bf906b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d2a8b46b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d483846b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d6246c6b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d7a6206b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3d8e6f06b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3da90086b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3dc0bfc6b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3ddaa606b000')
INSERT [dbo].[sys_role_menu_relation] ([fk_role_id], [fk_menu_id]) VALUES (N'041f43603452a000', N'041f3dfe23c6b000')
/****** Object:  Table [dbo].[sys_role]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_role](
	[pk_id] [varchar](32) NOT NULL,
	[uk_name] [varchar](60) NULL,
	[status] [varchar](1) NULL,
	[comment] [varchar](256) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK_system_role] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_system_role_name] UNIQUE NONCLUSTERED 
(
	[uk_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'唯一名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'uk_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态 1-启用,0-停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role'
GO
INSERT [dbo].[sys_role] ([pk_id], [uk_name], [status], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f43603452a000', N'系统管理员', N'1', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
/****** Object:  Table [dbo].[sys_org]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_org](
	[pk_id] [varchar](32) NOT NULL,
	[name] [varchar](60) NULL,
	[sort_no] [int] NULL,
	[leader] [varchar](60) NULL,
	[phonenumber] [varchar](15) NULL,
	[email] [varchar](100) NULL,
	[comment] [varchar](256) NULL,
	[fk_parent_id] [varchar](32) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK_system_org] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同级排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'sort_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'负责人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'leader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'phonenumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级机构ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'fk_parent_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组织机构' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org'
GO
INSERT [dbo].[sys_org] ([pk_id], [name], [sort_no], [leader], [phonenumber], [email], [comment], [fk_parent_id], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f45f86b86b000', N'IK-WEB', 1, N'', NULL, NULL, NULL, N'0', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
/****** Object:  Table [dbo].[sys_menu_api_relation]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sys_menu_api_relation](
	[fk_menu_id] [varchar](32) NOT NULL,
	[fk_api_uri] [varchar](256) NOT NULL,
 CONSTRAINT [PK_system_menu_api_relation] PRIMARY KEY CLUSTERED 
(
	[fk_menu_id] ASC,
	[fk_api_uri] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_api_relation', @level2type=N'COLUMN',@level2name=N'fk_menu_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'API-URI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_api_relation', @level2type=N'COLUMN',@level2name=N'fk_api_uri'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单API关系' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_api_relation'
GO
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412dd258dc6d000', N'/platform/dict/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412dd49e406d000', N'/platform/dict/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412ddc65906d000', N'/platform/dict/status/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412ddf008c6d000', N'/platform/dict/delete')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de04ca06d000', N'/platform/dict/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de20d006d000', N'/platform/dict-item/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de3be686d000', N'/platform/dict-item/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de556e06d000', N'/platform/dict-item/status/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de6db9c6d000', N'/platform/dict-item/delete')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412de88b386d000', N'/platform/dict-item/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412deb3db46d000', N'/platform/menu/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412df457486d000', N'/platform/menu/query')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412df457486d000', N'/platform/menu/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412df6feb06d000', N'/platform/menu/status/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412df878606d000', N'/platform/menu/delete')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412dfa57706d000', N'/platform/menu/tree/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'0412e0593346d000', N'/platform/test/son')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3cbe7146b000', N'/platform/org/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3cd515c6b000', N'/platform/org/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3cecda46b000', N'/platform/org/delete')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d0bf906b000', N'/platform/org/tree/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d2a8b46b000', N'/platform/menu/tree/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d2a8b46b000', N'/platform/role/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d483846b000', N'/platform/menu/tree/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d483846b000', N'/platform/role/query')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d483846b000', N'/platform/role/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d6246c6b000', N'/platform/role/status/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d7a6206b000', N'/platform/role/delete')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3d8e6f06b000', N'/platform/role/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3da90086b000', N'/platform/role/enabled/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3da90086b000', N'/platform/user/insert')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3dc0bfc6b000', N'/platform/user/query')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3dc0bfc6b000', N'/platform/role/enabled/list')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3dc0bfc6b000', N'/platform/user/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3ddaa606b000', N'/platform/user/status/update')
INSERT [dbo].[sys_menu_api_relation] ([fk_menu_id], [fk_api_uri]) VALUES (N'041f3dfe23c6b000', N'/platform/user/list')
/****** Object:  Table [dbo].[sys_menu]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_menu](
	[pk_id] [varchar](32) NOT NULL,
	[uk_code] [varchar](60) NULL,
	[name] [varchar](60) NULL,
	[type] [varchar](10) NULL,
	[sort_no] [int] NULL,
	[status] [varchar](1) NULL,
	[is_admin] [varchar](1) NULL,
	[fk_parent_id] [varchar](32) NULL,
	[comment] [varchar](256) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK_system_menu] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_system_menu_code] UNIQUE NONCLUSTERED 
(
	[uk_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'唯一编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'uk_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单类型, catagory-目录, menu-菜单, function-功能' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'sort_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态 1-启用,0-停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否管理员菜单, 1-是, 0-否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'is_admin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'fk_parent_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu'
GO
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040c2c86ba1a4000', N'platform', N'平台管理', N'category', 1, N'1', N'0', N'0', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea604af06a000', N'dict', N'字典管理', N'menu', 1, N'1', N'1', N'040c2c86ba1a4000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea6d535c6a000', N'dict-item', N'字典项管理', N'menu', 2, N'1', N'1', N'040c2c86ba1a4000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea6e00b06a000', N'menu', N'菜单管理', N'menu', 3, N'1', N'1', N'040c2c86ba1a4000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea6ead146a000', N'org', N'机构管理', N'menu', 4, N'1', N'0', N'040c2c86ba1a4000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea7022286a000', N'role', N'角色管理', N'menu', 5, N'1', N'0', N'040c2c86ba1a4000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040ea70f2586a000', N'user', N'用户管理', N'menu', 6, N'1', N'0', N'040c2c86ba1a4000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412dd258dc6d000', N'platform.dict.add', N'新增字典', N'function', 1, N'1', N'1', N'040ea604af06a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412dd49e406d000', N'platform.dict.edit', N'编辑字典', N'function', 2, N'1', N'1', N'040ea604af06a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412ddc65906d000', N'platform.dict.status.edit', N'启停字典', N'function', 3, N'1', N'1', N'040ea604af06a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412ddf008c6d000', N'platform.dict.delete', N'删除字典', N'function', 4, N'1', N'1', N'040ea604af06a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de04ca06d000', N'platform.dict.list', N'查询字典', N'function', 5, N'1', N'1', N'040ea604af06a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de20d006d000', N'platform.dict-item.add', N'新增字典项', N'function', 1, N'1', N'1', N'040ea6d535c6a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de3be686d000', N'platform.dict-item.edit', N'编辑字典项', N'function', 2, N'1', N'1', N'040ea6d535c6a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de556e06d000', N'platform.dict-item.status.edit', N'启停字典项', N'function', 3, N'1', N'1', N'040ea6d535c6a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de6db9c6d000', N'platform.dict-item.delete', N'删除字典项', N'function', 4, N'1', N'1', N'040ea6d535c6a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412de88b386d000', N'platform.dict-item.list', N'查询字典项', N'function', 5, N'1', N'1', N'040ea6d535c6a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412deb3db46d000', N'platform.menu.add', N'新增菜单', N'function', 1, N'1', N'1', N'040ea6e00b06a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412df457486d000', N'platform.menu.edit', N'编辑菜单', N'function', 2, N'1', N'1', N'040ea6e00b06a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412df6feb06d000', N'platform.menu.status.edit', N'启停菜单', N'function', 3, N'1', N'1', N'040ea6e00b06a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412df878606d000', N'platform.menu.delete', N'删除菜单', N'function', 4, N'1', N'1', N'040ea6e00b06a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'0412dfa57706d000', N'platform.menu.list', N'查询菜单', N'function', 5, N'1', N'1', N'040ea6e00b06a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3cbe7146b000', N'platform.org.add', N'新增机构', N'function', 1, N'1', N'0', N'040ea6ead146a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3cd515c6b000', N'platform.org.edit', N'编辑机构', N'function', 2, N'1', N'0', N'040ea6ead146a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3cecda46b000', N'platform.org.delete', N'删除机构', N'function', 3, N'1', N'0', N'040ea6ead146a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d0bf906b000', N'platform.org.list', N'查询机构', N'function', 4, N'1', N'0', N'040ea6ead146a000', N'', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d2a8b46b000', N'platform.role.add', N'新增角色', N'function', 1, N'1', N'0', N'040ea7022286a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d483846b000', N'platform.role.edit', N'编辑角色', N'function', 2, N'1', N'0', N'040ea7022286a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d6246c6b000', N'platform.role.status.edit', N'启停角色', N'function', 3, N'1', N'0', N'040ea7022286a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d7a6206b000', N'platform.role.delete', N'删除角色', N'function', 4, N'1', N'0', N'040ea7022286a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3d8e6f06b000', N'platform.role.list', N'查询角色', N'function', 5, N'1', N'0', N'040ea7022286a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3da90086b000', N'platform.user.add', N'新增用户', N'function', 1, N'1', N'0', N'040ea70f2586a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3dc0bfc6b000', N'platform.user.edit', N'编辑用户', N'function', 2, N'1', N'0', N'040ea70f2586a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3ddaa606b000', N'platform.user.status.edit', N'启停用户', N'function', 3, N'1', N'0', N'040ea70f2586a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_menu] ([pk_id], [uk_code], [name], [type], [sort_no], [status], [is_admin], [fk_parent_id], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'041f3dfe23c6b000', N'platform.user.list', N'查询用户', N'function', 4, N'1', N'0', N'040ea70f2586a000', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
/****** Object:  Table [dbo].[sys_dict_item]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_dict_item](
	[pk_id] [varchar](32) NOT NULL,
	[label] [varchar](60) NULL,
	[value] [varchar](60) NULL,
	[sort_no] [int] NULL,
	[status] [varchar](1) NULL,
	[comment] [varchar](256) NULL,
	[fk_dict_id] [varchar](32) NULL,
	[fk_dict_code] [varchar](60) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK_system_dict_item] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'显示文本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'label'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'排序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'sort_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态 1-启用,0-停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属字典PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'fk_dict_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属字典编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'fk_dict_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据字典项' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict_item'
GO
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'03fced468286d000', N'启用', N'1', 1, N'1', NULL, N'03fcd583fb86d000', N'data-status', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'03fced4e3646d000', N'停用', N'0', 2, N'1', NULL, N'03fcd583fb86d000', N'data-status', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'04004c326bd6f000', N'是', N'1', 1, N'1', NULL, N'04004c26b8d6f000', N'whether', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'04004c6d8bd6f000', N'否', N'0', 2, N'1', NULL, N'04004c26b8d6f000', N'whether', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040c197db2da4000', N'目录', N'category', 1, N'1', NULL, N'040c19469ada4000', N'menu-type', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040c198428da4000', N'菜单', N'menu', 1, N'1', NULL, N'040c19469ada4000', N'menu-type', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict_item] ([pk_id], [label], [value], [sort_no], [status], [comment], [fk_dict_id], [fk_dict_code], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040c198ade1a4000', N'功能', N'function', 1, N'1', NULL, N'040c19469ada4000', N'menu-type', N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
/****** Object:  Table [dbo].[sys_dict]    Script Date: 11/25/2021 20:43:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_dict](
	[pk_id] [varchar](32) NOT NULL,
	[uk_code] [varchar](60) NULL,
	[name] [varchar](60) NULL,
	[status] [varchar](1) NULL,
	[comment] [varchar](256) NULL,
	[create_by] [varchar](60) NULL,
	[create_time] [datetime] NULL,
	[update_by] [varchar](60) NULL,
	[update_time] [datetime] NULL,
 CONSTRAINT [PK_system_dict] PRIMARY KEY CLUSTERED 
(
	[pk_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_system_dict_code] UNIQUE NONCLUSTERED 
(
	[uk_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PK' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'pk_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'唯一编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'uk_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态 1-启用,0-停用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'create_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'create_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'update_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict', @level2type=N'COLUMN',@level2name=N'update_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据字典' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_dict'
GO
INSERT [dbo].[sys_dict] ([pk_id], [uk_code], [name], [status], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'03fcd583fb86d000', N'data-status', N'数据状态', N'1', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict] ([pk_id], [uk_code], [name], [status], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'04004c26b8d6f000', N'whether', N'是否项', N'1', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[sys_dict] ([pk_id], [uk_code], [name], [status], [comment], [create_by], [create_time], [update_by], [update_time]) VALUES (N'040c19469ada4000', N'menu-type', N'菜单类型', N'1', NULL, N'user-mock', CAST(0x0000AD5800000000 AS DateTime), NULL, NULL)
